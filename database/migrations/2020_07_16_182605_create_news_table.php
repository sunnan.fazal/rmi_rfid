<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string("news_image")->nullable();
            $table->text("news_image_description")->nullable();
            $table->text("news_details")->nullable();
            $table->string("news_image_1")->nullable();
            $table->string("news_image_2")->nullable();
            $table->string("news_image_3")->nullable();
            $table->string("news_image_4")->nullable();
            $table->text("news_last_description")->nullable();
            $table->integer("featured_news")->nullable();
            $table->integer("covid_news")->nullable();
            $table->integer("active");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
