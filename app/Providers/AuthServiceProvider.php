<?php

namespace App\Providers;

use App\Logins;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Logins::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerUserPolicies();
        //
    }

    public function registerUserPolicies()
    {
        Gate::define("is-candidate", "App\Policies\UserPolicy@candidate");
        Gate::define("is-admin", "App\Policies\UserPolicy@admin");
    }
}
