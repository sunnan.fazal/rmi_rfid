<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;
    public $user;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = (Auth::user() == null) ? null : Auth::user();
    }
    public function candidate()
    {
        return $this->user->TYPE == "CANDIDATE" ? true : false;
    }
    public function admin()
    {
        return $this->user->TYPE == "ADMIN" ? true : false;
    }
}
