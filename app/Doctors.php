<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctors extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'doctors';
    protected $fillable = ['doctor_image' , 'name' , 'title' , 'description'];
    public $timestamps = false;
}
