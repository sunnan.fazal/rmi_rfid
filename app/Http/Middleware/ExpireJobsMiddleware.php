<?php

namespace App\Http\Middleware;

use Facades\App\Http\Controllers\BackPortalController;
use Closure;

class ExpireJobsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        BackPortalController::validateJobs();
        return $next($request);
    }
}
