<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\BOG;
use App\Careers;
use App\Controllers;
use App\Doctors;
use App\Downloads;
use App\EmployeeDetails;
use App\Employees;
use App\FrontContentSection;
use App\Groups;
use App\MTI_Act;
use App\News;
use App\Slider;
use App\Tenders;
use App\Timeline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BackPortalController extends Controller
{

    public function Dashboard()
    {
        $tenders = Tenders::count();
        return view("admin.dashboard" , compact('tenders'));
    }

    public function showTenderGrid()
    {
        $tender = Tenders::orderBy('advertisement_date' , 'desc')->get();
        return view("admin.tender" , compact('tender'));
    }
    public function showCareersGrid()
    {
        $careers = Careers::orderBy('advertisement_date' , 'desc')->get();
        return view("admin.careers" , compact('careers'));
    }
    public function showCreateTender()
    {
        return view("admin.create-tender");
    }
    public function showCreateCareer()
    {
        return view("admin.create-career");
    }

    public function saveTender(Request $request)
    {
        $tender = new Tenders();
        $tender->tender_name = $request->get("tenderName");
        $tender->tender_type = $request->get("tenderType");
        $tender->procurement_entity = $request->get("procurement_entity");
        $tender->tender_description = $request->get("tenderDescription");
        if($request->file('file'))
        {
            $tender->tender_eoi = $request->file('file')->getClientOriginalName();
        }
        if($request->file('file_2'))
        {
            $tender->tender_eoi_2 = $request->file('file_2')->getClientOriginalName();
        }
        if($request->file('sbd'))
        {
            $tender->sbd = $request->file('sbd')->getClientOriginalName();
        }
        if($request->file('tender_evaluation'))
        {
            $tender->tender_evaluation = $request->file('tender_evaluation')->getClientOriginalName();
        }
        $tender->advertisement_date = $request->get("advertisement_date");
        $tender->closing_date = $request->get("closing_date");
        $tender->financial_remarks = $request->get("financialRemarks");
        $tender->active = $request->get("active") == "on" ? 1 : 0;
        $tender->save();
        if ($request->file('file'))
        {
            $request->file('file')->move(public_path('../resources/tenders/'), $request->file('file')->getClientOriginalName());
        }
        if ($request->file('file_2'))
        {
            $request->file('file_2')->move(public_path('../resources/tenders/'), $request->file('file_2')->getClientOriginalName());
        }
        if ($request->file('sbd'))
        {
            $request->file('sbd')->move(public_path('../resources/tenders/'), $request->file('sbd')->getClientOriginalName());
        }
        if ($request->file('tender_evaluation'))
        {
            $request->file('tender_evaluation')->move(public_path('../resources/tenders/'), $request->file('tender_evaluation')->getClientOriginalName());
        }
        return redirect()->to("/tenders");
    }

    public function saveEditedTender(Request $request)
    {
        $tender = Tenders::find($request->get('tender_id'));
        $tender->tender_name = $request->get("tenderName");
        $tender->tender_type = $request->get("tenderType");
        $tender->procurement_entity = $request->get("procurement_entity");
        $tender->tender_description = $request->get("tenderDescription");
        if($request->file('file'))
        {
            $tender->tender_eoi = $request->file('file')->getClientOriginalName();
        }
        if($request->file('file_2'))
        {
            $tender->tender_eoi_2 = $request->file('file_2')->getClientOriginalName();
        }
        if($request->file('sbd'))
        {
            $tender->sbd = $request->file('sbd')->getClientOriginalName();
        }
        if($request->file('tender_evaluation'))
        {
            $tender->tender_evaluation = $request->file('tender_evaluation')->getClientOriginalName();
        }
        $tender->advertisement_date = $request->get("advertisement_date");
        $tender->closing_date = $request->get("closing_date");
        $tender->financial_remarks = $request->get("financialRemarks");
        $tender->active = $request->get("active") == "on" ? 1 : 0;
        $tender->save();
        if ($request->file('file'))
        {
            $request->file('file')->move(public_path('../resources/tenders/'), $request->file('file')->getClientOriginalName());
        }
        if ($request->file('file_2'))
        {
            $request->file('file_2')->move(public_path('../resources/tenders/'), $request->file('file_2')->getClientOriginalName());
        }
        if ($request->file('sbd'))
        {
            $request->file('sbd')->move(public_path('../resources/tenders/'), $request->file('sbd')->getClientOriginalName());
        }
        if ($request->file('tender_evaluation'))
        {
            $request->file('tender_evaluation')->move(public_path('../resources/tenders/'), $request->file('tender_evaluation')->getClientOriginalName());
        }
        return redirect()->to("/tenders");
    }

    public function editTender($id)
    {
        $tender = Tenders::find($id);
        return view("admin.edit-tender" , compact('tender'));
    }
    public function editCareer($id)
    {
        $career = Careers::find($id);
        return view("admin.edit-career" , compact('career'));
    }

    public function saveAdvertisement(Request $request)
    {
        $tender = new Careers();
        $tender->type = $request->get("career_type");
        $tender->description = $request->get("career_description");
        $tender->advertisement_date = $request->get("advertisement_date");
        $tender->closing_date = $request->get("closing_date");
        if($request->file('file'))
        {
            $tender->file = $request->file('file')->getClientOriginalName();
        }
        $tender->active = $request->get("active") == "on" ? 1 : 0;
        $tender->save();
        if ($request->file('file'))
        {
            $request->file->move(public_path('../resources/careers/'), $request->file('file')->getClientOriginalName());
        }
        return redirect()->to("/careers");
    }

    public function saveEditAdvertisement(Request $request)
    {
        $tender = Careers::find($request->get('career_id'));
        $tender->type = $request->get("career_type");
        $tender->description = $request->get("career_description");
        $tender->advertisement_date = $request->get("advertisement_date");
        $tender->closing_date = $request->get("closing_date");
        if($request->file('file'))
        {
            $tender->file = $request->file('file')->getClientOriginalName();
        }
        $tender->active = $request->get("active") == "on" ? 1 : 0;
        $tender->save();
        if ($request->file('file'))
        {
            $request->file->move(public_path('../resources/careers/'), $request->file('file')->getClientOriginalName());
        }
        return redirect()->to("/careers");
    }

    public function showMtiActs()
    {
        $mti_act = MTI_Act::where('active' , 1)->get();
        return view("admin.mti-acts" , compact('mti_act'));
    }
    public function createMTIActs()
    {
        return view("admin.create-mti-acts");
    }
    public function showDownloads()
    {
        $download = Downloads::all();
        return view("admin.all-downloads" , compact('download'));
    }
    public function createDownloads()
    {
        return view("admin.create-downloads");
    }
    public function editMTIActs($id)
    {
        $mti_act = MTI_Act::find($id);
        return view("admin.edit-mti_act" , compact('mti_act'));
    }
    public function removeDownload($id)
    {
        $download = Downloads::find($id);
        $download->delete();
        return redirect()->to("/downloads");
    }
    public function saveMTIActs(Request $request)
    {
        $tender = new MTI_Act();
        $tender->description = $request->get("description");
        if($request->file('file'))
        {
            $tender->file = $request->file('file')->getClientOriginalName();
        }
        $tender->active = $request->get("active") == "on" ? 1 : 0;
        $tender->save();
        if ($request->file('file'))
        {
            $request->file->move(public_path('../resources/mti_acts/'), $request->file('file')->getClientOriginalName());
        }
        return redirect()->to("/mti-acts");
    }
    public function saveEditMTIActs(Request $request)
    {
        $tender = MTI_Act::find($request->get('mti_id'));
        $tender->description = $request->get("description");
        if($request->file('file'))
        {
            $tender->file = $request->file('file')->getClientOriginalName();
        }
        $tender->active = $request->get("active") == "on" ? 1 : 0;
        $tender->save();
        if ($request->file('file'))
        {
            $request->file->move(public_path('../resources/mti_acts/'), $request->file('file')->getClientOriginalName());
        }
        return redirect()->to("/mti-acts");
    }

    public function saveDownloads(Request $request)
    {
        $tender = new Downloads();
        $tender->name = $request->get("name");
        $tender->description = $request->get("description");
        if($request->file('file'))
        {
            $tender->file = $request->file('file')->getClientOriginalName();
        }
        $tender->save();
        if ($request->file('file'))
        {
            $request->file->move(public_path('../resources/downloads/'), $request->file('file')->getClientOriginalName());
        }
        return redirect()->to("/downloads");
    }

    public function showNews()
    {
        $news = News::orderByDesc('updated_at')->get();
        return view("admin.news" , compact('news'));
    }
    public function editNews($id)
    {
        $news = News::where("id" , $id)->first();
        return view("admin.edit-news" , compact('news'));
    }

    public function createNews()
    {
        return view("admin.create-news");
    }

    public function saveNews(Request $request)
    {
        $news = new News();
        if($request->file('pic_1'))
        {
            $news->news_image = $request->file('pic_1')->getClientOriginalName();
              $request->file('pic_1')->move(public_path('../resources/news/'), $request->file('pic_1')->getClientOriginalName());
        }
        if($request->file('news_pic1'))
        {
            $news->news_image_1 = $request->file('news_pic1')->getClientOriginalName();
            $request->file('news_pic1')->move(public_path('../resources/news/'), $request->file('news_pic1')->getClientOriginalName());
        }
        if($request->file('news_pic2'))
        {
            $news->news_image_2 = $request->file('news_pic2')->getClientOriginalName();
            $request->file('news_pic2')->move(public_path('../resources/news/'), $request->file('news_pic2')->getClientOriginalName());
        }
        if($request->file('news_pic3'))
        {
            $news->news_image_3 = $request->file('news_pic3')->getClientOriginalName();
            $request->file('news_pic3')->move(public_path('../resources/news/'), $request->file('news_pic3')->getClientOriginalName());
        }
        if($request->file('news_pic4'))
        {
            $news->news_image_4 = $request->file('news_pic4')->getClientOriginalName();
            $request->file('news_pic4')->move(public_path('../resources/news/'), $request->file('news_pic4')->getClientOriginalName());
        }

        $news->news_image_description = $request->get("newsDescription1");
        $news->news_details = $request->get("newsDescription2");
        $news->news_last_description = $request->get("newsDescription3");
        $news->featured_news = ($request->get("newstype") == "Featured News") ? 1 : 0;
        $news->covid_news = ($request->get("newstype") == "COVID-19 News") ? 1 : 0;
        $news->active = $request->get("active") == "on" ? 1 : 0;
        $news->save();

        return redirect()->to("/news");
    }

    public function saveEditNews(Request $request)
    {

        $news = News::find($request->get('newsid'));
        if($request->file('pic_1'))
        {
            $news->news_image = $request->file('pic_1')->getClientOriginalName();
              $request->file('pic_1')->move(public_path('../resources/news/'), $request->file('pic_1')->getClientOriginalName());
        }
        if($request->file('news_pic1'))
        {
            $news->news_image_1 = $request->file('news_pic1')->getClientOriginalName();
            $request->file('news_pic1')->move(public_path('../resources/news/'), $request->file('news_pic1')->getClientOriginalName());
        }
        if($request->file('news_pic2'))
        {
            $news->news_image_2 = $request->file('news_pic2')->getClientOriginalName();
            $request->file('news_pic2')->move(public_path('../resources/news/'), $request->file('news_pic2')->getClientOriginalName());
        }
        if($request->file('news_pic3'))
        {
            $news->news_image_3 = $request->file('news_pic3')->getClientOriginalName();
            $request->file('news_pic3')->move(public_path('../resources/news/'), $request->file('news_pic3')->getClientOriginalName());
        }
        if($request->file('news_pic4'))
        {
            $news->news_image_4 = $request->file('news_pic4')->getClientOriginalName();
            $request->file('news_pic4')->move(public_path('../resources/news/'), $request->file('news_pic4')->getClientOriginalName());
        }

        $news->news_image_description = $request->get("newsDescription1");
        $news->news_details = $request->get("newsDescription2");
        $news->news_last_description = $request->get("newsDescription3");
        $news->featured_news = ($request->get("newstype") == "Featured News") ? 1 : 0;
        $news->covid_news = ($request->get("newstype") == "COVID-19 News") ? 1 : 0;
        $news->active = $request->get("active") == "on" ? 1 : 0;
        $news->save();

        return redirect()->to("/news");
    }
    
    public function saveFrontContent(Request $request)
    {
        $content = FrontContentSection::where("content_level" , $request->get("level"))->get();
        if(count($content) > 0)
        {
            // Already Exist Just Update the content
            
            $content = $content->first();
            $content->content_level = $request->get("level");
            if($request->file('pic'))
            {
                $content->content_pic = $request->get("level").'-'.$request->file('pic')->getClientOriginalName();
                $request->file('pic')->move(public_path('../resources/contents/'), $request->get("level").'-'.$request->file('pic')->getClientOriginalName());
            }
            
             $content->content_description = $request->get("contentDescription");
             $content->save();
        }
        else
        {
            // No Content, Create
            
            $content = new FrontContentSection();
            $content->content_level = $request->get("level");
            if($request->file('pic'))
            {
                $content->content_pic = $request->get("level").'-'.$request->file('pic')->getClientOriginalName();
                $request->file('pic')->move(public_path('../resources/contents/'), $request->get("level").'-'.$request->file('pic')->getClientOriginalName());
            }
            
             $content->content_description = $request->get("contentDescription");
             $content->save();
            
        }
        return redirect()->back();
    }
    
    public function showFrontContents()
    {
        $first_content = FrontContentSection::where("content_level",1)->get();
        $second_content = FrontContentSection::where("content_level",2)->get();
        $third_content = FrontContentSection::where("content_level",3)->get();
        return view("admin.front-contents" , compact('first_content','second_content','third_content'));
    }

    public function showSliders()
    {
        $slider = Slider::all();
        return view("admin.slider" , compact('slider'));
    }
    public function createSlider()
    {
        return view("admin.create-slider");
    }
    public function saveSlider(Request $request)
    {
        $content = new Slider();
        $content->slider_text = $request->get("sliderText");
        if($request->file('pic'))
        {
            $content->slider_image = $request->file('pic')->getClientOriginalName();
            $request->file('pic')->move(public_path('../resources/sliders/'), $request->file('pic')->getClientOriginalName());
        }
        $content->save();
        
        return redirect()->to("/sliders");       
    }
    public function editSaveSlider(Request $request)
    {
        $content = Slider::where('id' , $request->get('sliderid'))->first();
        $content->slider_text = $request->get("sliderText");
        if($request->file('pic'))
        {
            $content->slider_image = $request->file('pic')->getClientOriginalName();
            $request->file('pic')->move(public_path('../resources/sliders/'), $request->file('pic')->getClientOriginalName());
        }
        $content->save();
        
        return redirect()->to("/sliders");       
    }

    public function editSlider($id)
    {
        $slider = Slider::where('id' , $id)->first();
        return view("admin.edit-slider" , compact('slider'));
    }

    public function showDoctors()
    {
        $doctor = Doctors::all();
        return view("admin.doctors" , compact('doctor'));
    }
    public function createDoctor()
    {
        return view("admin.create-doctor");
    }

    public function saveDoctor(Request $request)
    {
        $doctor = new Doctors();
        $doctor->name = $request->get("name");
        $doctor->title = $request->get("title");
        $doctor->description = $request->get("description");
        if($request->file('pic'))
        {
            $doctor->doctor_image = $request->file('pic')->getClientOriginalName();
            $request->file('pic')->move(public_path('../resources/doctors/'), $request->file('pic')->getClientOriginalName());
        }
        $doctor->save();
        
        return redirect()->to("/doctors");   
    }

    public function editDoctor($id)
    {
        $doctor = Doctors::where('id' , $id)->first();
        return view("admin.edit-doctor", compact('doctor'));
    }

    public function editSaveDoctor(Request $request)
    {
        $doctor = Doctors::where('id' , $request->get('doctorid'))->first();
        $doctor->name = $request->get("name");
        $doctor->title = $request->get("title");
        $doctor->description = $request->get("description");
        if($request->file('pic'))
        {
            $doctor->doctor_image = $request->file('pic')->getClientOriginalName();
            $request->file('pic')->move(public_path('../resources/doctors/'), $request->file('pic')->getClientOriginalName());
        }
        $doctor->save();
        
        return redirect()->to("/doctors");   
    }

    public function aboutUs()
    {
        $aboutus = AboutUs::where('type' ,"about-us")->get();
        return view("admin.about-us" , compact('aboutus'));
    }
    public function visionMission()
    {
        $aboutus = AboutUs::where('type' ,"vision-mission")->get();
        return view("admin.vision-mission" , compact('aboutus'));
    }

    public function saveAboutUs(Request $request)
    {

        $aboutUs = AboutUs::where("type" , 'about-us')->get();
        if(count($aboutUs) > 0)
        {   
            $aboutUs = $aboutUs->first();
        }
        else
        {
            $aboutUs = new AboutUs();    
        }
        $aboutUs->type = "about-us";
        if($request->file('banner_image'))
        {
            $aboutUs->banner_image = $request->file('banner_image')->getClientOriginalName();
            $request->file('banner_image')->move(public_path('../resources/aboutus/'), $request->file('banner_image')->getClientOriginalName());
        }
        $aboutUs->details = $request->get('details');

        if($request->file('image_1'))
        {
            $aboutUs->image_1 = $request->file('image_1')->getClientOriginalName();
            $request->file('image_1')->move(public_path('../resources/aboutus/'), $request->file('image_1')->getClientOriginalName());
        }
        if($request->file('image_2'))
        {
            $aboutUs->image_2 = $request->file('image_2')->getClientOriginalName();
            $request->file('image_2')->move(public_path('../resources/aboutus/'), $request->file('image_2')->getClientOriginalName());
        }
        if($request->file('image_3'))
        {
            $aboutUs->image_3 = $request->file('image_3')->getClientOriginalName();
            $request->file('image_3')->move(public_path('../resources/aboutus/'), $request->file('image_3')->getClientOriginalName());
        }
        $aboutUs->details_2 = $request->get('details_2');

        if($request->file('image_4'))
        {
            $aboutUs->image_4 = $request->file('image_4')->getClientOriginalName();
            $request->file('image_4')->move(public_path('../resources/aboutus/'), $request->file('image_4')->getClientOriginalName());
        }
        if($request->file('image_5'))
        {
            $aboutUs->image_5 = $request->file('image_5')->getClientOriginalName();
            $request->file('image_5')->move(public_path('../resources/aboutus/'), $request->file('image_5')->getClientOriginalName());
        }
        if($request->file('image_6'))
        {
            $aboutUs->image_6 = $request->file('image_6')->getClientOriginalName();
            $request->file('image_6')->move(public_path('../resources/aboutus/'), $request->file('image_6')->getClientOriginalName());
        }
        $aboutUs->details_3 = $request->get('details_3');
        if($request->file('image_7'))
        {
            $aboutUs->image_7 = $request->file('image_7')->getClientOriginalName();
            $request->file('image_7')->move(public_path('../resources/aboutus/'), $request->file('image_7')->getClientOriginalName());
        }
        if($request->file('image_8'))
        {
            $aboutUs->image_8 = $request->file('image_8')->getClientOriginalName();
            $request->file('image_8')->move(public_path('../resources/aboutus/'), $request->file('image_8')->getClientOriginalName());
        }
        if($request->file('image_9'))
        {
            $aboutUs->image_9 = $request->file('image_9')->getClientOriginalName();
            $request->file('image_9')->move(public_path('../resources/aboutus/'), $request->file('image_9')->getClientOriginalName());
        }

        $aboutUs->save();
        
        return redirect()->to("/about-us"); 
    }
    public function saveVisionMission(Request $request)
    {

        $aboutUs = AboutUs::where("type" , 'vision-mission')->get();
        if(count($aboutUs) > 0)
        {   
            $aboutUs = $aboutUs->first();
        }
        else
        {
            $aboutUs = new AboutUs();    
        }
        $aboutUs->type = "vision-mission";
        if($request->file('banner_image'))
        {
            $aboutUs->banner_image = $request->file('banner_image')->getClientOriginalName();
            $request->file('banner_image')->move(public_path('../resources/vision-mission/'), $request->file('banner_image')->getClientOriginalName());
        }
        $aboutUs->details = $request->get('details');

        if($request->file('image_1'))
        {
            $aboutUs->image_1 = $request->file('image_1')->getClientOriginalName();
            $request->file('image_1')->move(public_path('../resources/vision-mission/'), $request->file('image_1')->getClientOriginalName());
        }
        if($request->file('image_2'))
        {
            $aboutUs->image_2 = $request->file('image_2')->getClientOriginalName();
            $request->file('image_2')->move(public_path('../resources/vision-mission/'), $request->file('image_2')->getClientOriginalName());
        }
        if($request->file('image_3'))
        {
            $aboutUs->image_3 = $request->file('image_3')->getClientOriginalName();
            $request->file('image_3')->move(public_path('../resources/vision-mission/'), $request->file('image_3')->getClientOriginalName());
        }
        $aboutUs->details_2 = $request->get('details_2');

        if($request->file('image_4'))
        {
            $aboutUs->image_4 = $request->file('image_4')->getClientOriginalName();
            $request->file('image_4')->move(public_path('../resources/vision-mission/'), $request->file('image_4')->getClientOriginalName());
        }
        if($request->file('image_5'))
        {
            $aboutUs->image_5 = $request->file('image_5')->getClientOriginalName();
            $request->file('image_5')->move(public_path('../resources/vision-mission/'), $request->file('image_5')->getClientOriginalName());
        }
        if($request->file('image_6'))
        {
            $aboutUs->image_6 = $request->file('image_6')->getClientOriginalName();
            $request->file('image_6')->move(public_path('../resources/vision-mission/'), $request->file('image_6')->getClientOriginalName());
        }
        $aboutUs->details_3 = $request->get('details_3');
        if($request->file('image_7'))
        {
            $aboutUs->image_7 = $request->file('image_7')->getClientOriginalName();
            $request->file('image_7')->move(public_path('../resources/vision-mission/'), $request->file('image_7')->getClientOriginalName());
        }
        if($request->file('image_8'))
        {
            $aboutUs->image_8 = $request->file('image_8')->getClientOriginalName();
            $request->file('image_8')->move(public_path('../resources/vision-mission/'), $request->file('image_8')->getClientOriginalName());
        }
        if($request->file('image_9'))
        {
            $aboutUs->image_9 = $request->file('image_9')->getClientOriginalName();
            $request->file('image_9')->move(public_path('../resources/vision-mission/'), $request->file('image_9')->getClientOriginalName());
        }

        $aboutUs->save();
        
        return redirect()->to("/vision-mission"); 
    }

    public function governorsBoard()
    {
        $bog = BOG::all();
        return view("admin.governors-board" , compact('bog'));
    }
    public function createGovernorsBoard()
    {
        return view("admin.create-board-governor");
    }
    public function editBOG($id)
    {
        $bog = BOG::where('id' , $id)->first();
        return view("admin.edit-board-governor" , compact('bog'));
    }
    public function saveBOG(Request $request)
    {
        $bog = new BOG();
        $bog->bog_name = $request->get("bog_name");
        $bog->bog_title = $request->get("bog_title");
        if($request->file('bog_image'))
        {
            $bog->bog_image = $request->file('bog_image')->getClientOriginalName();
            $request->file('bog_image')->move(public_path('../resources/bog/'), $request->file('bog_image')->getClientOriginalName());
        }
        $bog->save();
        
        return redirect()->to("/governors");   
    }
    public function saveEditBOG(Request $request)
    {
        $bog = BOG::where('id' , $request->get('bogid'))->first();
        $bog->bog_name = $request->get("bog_name");
        $bog->bog_title = $request->get("bog_title");
        if($request->file('bog_image'))
        {
            $bog->bog_image = $request->file('bog_image')->getClientOriginalName();
            $request->file('bog_image')->move(public_path('../resources/bog/'), $request->file('bog_image')->getClientOriginalName());
        }
        $bog->save();
        
        return redirect()->to("/governors");   
    }
    public function timeline()
    {
        $timeline = Timeline::all();
        return view("admin.timeline" , compact('timeline'));
    }
    public function editTimeline($id)
    {
        $timeline = Timeline::where('id' , $id)->first();
        return view("admin.edit-timeline" , compact('timeline'));
    }

    public function createTimeline()
    {
        return view("admin.create-timeline");
    }

    public function saveTimeline(Request $request)
    {
        $timeline = new Timeline();
        if($request->file('image'))
        {
            $timeline->image = $request->file('image')->getClientOriginalName();
            $request->file('image')->move(public_path('../resources/timeline/'), $request->file('image')->getClientOriginalName());
        }
        $timeline->title = $request->get("title");
        $timeline->description = $request->get("description");
        $timeline->created_at = $request->get("date");
        $timeline->save();
        return redirect()->to("timeline");
    }

    public function SaveEditTimeline(Request $request)
    {
        $timeline = Timeline::where('id' , $request->get('timelineid'))->first();
        if($request->file('image'))
        {
            $timeline->image = $request->file('image')->getClientOriginalName();
            $request->file('image')->move(public_path('../resources/timeline/'), $request->file('image')->getClientOriginalName());
        }
        $timeline->title = $request->get("title");
        $timeline->description = $request->get("description");
        $timeline->created_at = $request->get("date");
        $timeline->save();
        return redirect()->to("timeline");
    }

    public function getCurrentDateTime()
    {
        return date("Y-m-d");
    }

    public function getCurrentID()
    {
        return Auth::user()->ID;
    }

}
