-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 20, 2020 at 04:49 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cardiology`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE `aboutus` (
  `id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `banner_image` varchar(100) DEFAULT NULL,
  `details` longtext DEFAULT NULL,
  `image_1` varchar(100) DEFAULT NULL,
  `image_2` varchar(100) DEFAULT NULL,
  `image_3` varchar(100) DEFAULT NULL,
  `details_2` longtext DEFAULT NULL,
  `image_4` varchar(100) DEFAULT NULL,
  `image_5` varchar(100) DEFAULT NULL,
  `image_6` varchar(100) DEFAULT NULL,
  `details_3` longtext DEFAULT NULL,
  `image_7` varchar(100) DEFAULT NULL,
  `image_8` varchar(100) DEFAULT NULL,
  `image_9` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aboutus`
--

INSERT INTO `aboutus` (`id`, `type`, `banner_image`, `details`, `image_1`, `image_2`, `image_3`, `details_2`, `image_4`, `image_5`, `image_6`, `details_3`, `image_7`, `image_8`, `image_9`) VALUES
(1, 'about-us', 'bg12.jpg', '<h1><span style=\"color:#c0392b\"><strong><span style=\"font-size:28px\">Who We Are?</span></strong></span></h1>\r\n\r\n<h3><strong>Overview</strong></h3>\r\n\r\n<p>Peshawar Institute of Cardiology (PIC) commenced its operations in 2020. It has come a long way in just a few years of its operations. The Institute has grown tremendously in the facilities it has to offer and can now boast to be one of the best-equipped hospitals in the region.<br />\r\nRehman Medical Institute has revolutionized the concept of medical care in the KP Hayatabad, Peshawar. The 200+ bedded centrally air-conditioned building is a state of the art tertiary care medical facility providing preventive, curative and rehabilitative services to the people of Pakistan and Afghanistan. The people of Peshawar, its surrounding areas and Afghanistan, for the first time ever, have access to quality medical and healthcare facilities of international standards.<br />\r\n&nbsp;</p>\r\n\r\n<h2>MEDICAL SERVICES</h2>\r\n\r\n<ul>\r\n	<li>Cardiology</li>\r\n	<li>Neurology</li>\r\n	<li>Pulmonology</li>\r\n	<li>Paediatrics &amp; Neonatology&nbsp;</li>\r\n	<li>Physiotherapy &amp; Rehabilitation</li>\r\n	<li>Oncology</li>\r\n	<li>General Medicine</li>\r\n	<li>Gastroenterology</li>\r\n	<li>Nephrology</li>\r\n	<li>Dermatology</li>\r\n	<li>Psychiatry</li>\r\n	<li>\r\n	<h2>SURGICAL SERVICES</h2>\r\n	</li>\r\n	<li>Cardiothoracic Surgery</li>\r\n	<li>Cardio-Vascular Surgery</li>\r\n	<li>Gynaecology /Obstetrics</li>\r\n	<li>Centre for Reproductive Medicine</li>\r\n	<li>Paediatric surgery</li>\r\n	<li>Ophthalmology</li>\r\n	<li>Dentistry</li>\r\n	<li>Oral and Maxillofacial Surgery</li>\r\n	<li>Orthopaedic Surgery</li>\r\n	<li>Plastic surgery</li>\r\n	<li>Neuro &amp; Spine Surgery</li>\r\n	<li>General Surgery</li>\r\n	<li>Urology</li>\r\n	<li>ENT</li>\r\n	<li>Kidney Transplantation</li>\r\n</ul>\r\n\r\n<h2>DIAGNOSTIC SERVICES</h2>\r\n\r\n<ul>\r\n	<li>Angiography</li>\r\n	<li>Neurology</li>\r\n	<li>Radiology &amp; imaging</li>\r\n	<li>Pathology</li>\r\n	<li>Haematology Daycare</li>\r\n	<li>Cardiology Echo, ECG, ETT, Holter monitoring etc.</li>\r\n</ul>\r\n\r\n<p>We offer quality support services to patients for providing ease during their indoor treatments and hospital visits. Patients can request to access their digitalized health records, laboratory diagnostic reports and confirmation of consultant appointments through SMS.<br />\r\nRecently, we have launched Home Health Services to provide rehabilitation services at the doorstep of the patients.</p>', 'ward4.jpg', 'ward_3.jpg', 'ward_2.jpg', '<h3><strong>PIC Mission Statement</strong></h3>\r\n\r\n<h1>OUR MISSION</h1>\r\n\r\n<blockquote>\r\n<h1>The PIC is committed to serve the region by providing:</h1>\r\n</blockquote>\r\n\r\n<ul>\r\n	<li>Quality Healthcare Services through state of the art diagnostic facilities and treatment of the highest possible standard in a comfortable, caring and safe environment.</li>\r\n	<li>Career Opportunities in an environment providing our employees with a purpose and pride in the organization, through personal development and recognition for performance excellence.</li>\r\n	<li>Education and Training to health professionals while inculcating in them the ethics and values of professional modes of practice.</li>\r\n</ul>', 'heart.png', 'heart.jpg', '6.jpg', NULL, 'bg7.jpg', NULL, NULL),
(2, 'vision-mission', 'bg2.jpg', '<h1><span style=\"color:#c0392b\"><span style=\"font-size:36px\"><strong>Vision &amp; Mission</strong></span></span></h1>\r\n\r\n<h2><strong>PIC&#39;s&nbsp;Vision Statement</strong></h2>\r\n\r\n<h2>OUR VISION</h2>\r\n\r\n<p>To be the premiere healthcare provider of the region, focusing on provision of:</p>\r\n\r\n<ul>\r\n	<li>Quality Healthcare Services</li>\r\n	<li>Career Opportunities</li>\r\n	<li>Education and Training</li>\r\n</ul>', 'contactus.png', 'downloads.jpg', 'rules-and-regulations.jpg', '<h2>PIC&#39;s Mission Statement</h2>\r\n\r\n<h1><strong>OUR MISSION</strong></h1>\r\n\r\n<blockquote>\r\n<p><span style=\"font-size:20px\">The Peshawar Institute of Cardiology&nbsp;is committed to serve the region by providing:</span></p>\r\n</blockquote>\r\n\r\n<ul>\r\n	<li>Quality Healthcare Services through state of the art diagnostic facilities and treatment of the highest possible standard in a comfortable, caring and safe environment.</li>\r\n	<li>Career Opportunities in an environment providing our employees with a purpose and pride in the organization, through personal development and recognition for performance excellence.</li>\r\n	<li>Education and Training to health professionals while inculcating in them the ethics and values of professional modes of practice.</li>\r\n</ul>', 'card.jpg', 'logo.jpg', 'pedriatic-cardiology.jpg', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bog`
--

CREATE TABLE `bog` (
  `id` int(11) NOT NULL,
  `bog_image` varchar(100) DEFAULT NULL,
  `bog_name` varchar(200) DEFAULT NULL,
  `bog_title` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bog`
--

INSERT INTO `bog` (`id`, `bog_image`, `bog_name`, `bog_title`) VALUES
(1, 'DSC_0116.JPG', 'Sunnan Fazal Azeem', 'Chief Executive Officer'),
(2, 'DSC_0114.JPG', 'Board of Governor 2', 'Director Finance');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(11) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `advertisement_date` varchar(50) DEFAULT NULL,
  `closing_date` varchar(50) DEFAULT NULL,
  `file` varchar(1000) DEFAULT NULL,
  `active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `type`, `description`, `advertisement_date`, `closing_date`, `file`, `active`) VALUES
(1, 'Contract', 'Job Application PIC', '2020-07-07', '2020-07-21', 'advertisement.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `doctor_image` varchar(100) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `title` varchar(500) DEFAULT NULL,
  `description` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `doctor_image`, `name`, `title`, `description`) VALUES
(1, 'DSC_0145.JPG', 'Sunnan Fazal Azeem', 'Cheif Cyber Security officer', '<p>bhdfsahkljfhjk hjklsadhfsdjkfh kjsfhklj kljwqieur werwiejksdfjsdkljfhljsdfjsdjf jkhsdjklfh lsdfjksjkdfh jksdfh jklsdhlkfjsjkdlf hjklsdfhjklshdfjkl hsdjklfhkjsdlfhkjlsdhfjkhsdfhsdhfkhskjlfhwhkrhwjekrh wer</p>\r\n\r\n<p>wefhbsadfkjlhsdfjklhsdkjlfhjkshfd jk jk kj hjk sdhfkjlsdhf jklhsdj klfsdjklf hjklsdhfkjlsdhfjkl hsdkjlfhsjkdfsdhfjkhweriweuriopweuroiuioprusdiof sdfihwjelkj rhd</p>\r\n\r\n<p>fasdfksdfhkjsdhfkjlhsdf</p>\r\n\r\n<p>sdfksdf jkhsjfkhsfhjkl;sdhfklj;shdfjklhskldjfhklwjhjklerhwerhiuoweroiweruioriosdfhsdlkjfhjklsdhfjwe;rhjkwerhjklwerhljkshdfjkhsjkflhsdljkfs</p>\r\n\r\n<p>df&nbsp;</p>\r\n\r\n<p>sd</p>\r\n\r\n<p>fsdfsdfsdfjhsdakfjhs;adfj sdakf jlsda</p>'),
(2, 'DSC_0012.JPG', 'Adeel Wahab', 'Director Administration', '<p>awerwoeiru iopwejsadfjshdfjhsdjklfljkjkhrjkl jkfhlsdjkf hkljwehjklrhljkhsjlk fhsldkjfhjkljklewhrjklwehrfiousiopdfiopwequoripweiorhdsjhkjlsdfhklhlkwqehkb&nbsp; ndfsdf sadnf&nbsp; sf sd f sdf w e r f sd f&nbsp; we r wer wer</p>\r\n\r\n<p>ewfsd</p>\r\n\r\n<p>f</p>\r\n\r\n<p>sdaf</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>sf</p>\r\n\r\n<p>sd</p>\r\n\r\n<p>r</p>\r\n\r\n<p>we</p>\r\n\r\n<p>r</p>\r\n\r\n<p>wqer</p>\r\n\r\n<p>wqerewqwqe</p>');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`id`, `name`, `description`, `file`) VALUES
(4, 'Job Application PIC', 'Job Form for applying in PIC', 'application.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `front_content_section`
--

CREATE TABLE `front_content_section` (
  `id` int(11) NOT NULL,
  `content_level` int(11) DEFAULT NULL,
  `content_pic` varchar(100) DEFAULT NULL,
  `content_description` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `front_content_section`
--

INSERT INTO `front_content_section` (`id`, `content_level`, `content_pic`, `content_description`) VALUES
(3, 1, '1-staff.jpg', '<h2><strong>Cardiology</strong></h2>\r\n\r\n<p><span style=\"font-size:16px\">The field of cardiology deals with diseases, abnormalities, and&nbsp;disorders of the human cardiovascular system (heart and blood vessels). Cardiologists &amp; Cardiovascular surgeons are professionals who specialize in diagnosis and treatment of conditions such as coronary artery disease, valvular heart disease, angina pectoris, heart failure, congenital heart defects, electrophysiology of the heart, etc. Cardiovascular diseases (CVDs) are among leading causes of sudden death and persistent health risks for Pakistani population. However, emergence of advanced techniques, innovative devices and procedures for diagnosing and treating heart conditions have led to new and effective treatment options for those suffering from complex heart conditions.</span></p>'),
(4, 2, '2-PIC-Building.jpg', '<p><span style=\"color:#000000\"><span style=\"font-size:28px\"><strong>WELCOME</strong></span></span></p>\r\n\r\n<p>Peshawar Institute of Cardiology Heart &amp; Vascular Center provides the most advanced and comprehensive care for cardiac, vascular and pulmonary problems in Pakistan. A full time, devoted team of experienced Cardiologists, Cardiac surgeons, Anesthetists, OR &amp; Cath lab technicians, Nurses, and Pharmacists work together to provide an integrated &amp; individualized care to every patient. The department offers a wide range of clinical services as well as diagnostic, therapeutic and rehabilitation facilities for the patients, marking a milestone in the history of PIC as Cardiac Catheterization Lab and Heart Bypass Services were established. PIC Emergency services are also contributing by reducing fatality risk significantly in heart attach/ chest pain patients with timely, quality, 24/7 care.<br />\r\n&nbsp;</p>'),
(5, 3, '3-banner_umc_surgical_day_1_3275_0.jpg', '<p><span style=\"font-size:20px\"><strong>A PEDIATRIC CARDIOLOGIST MAY PERFORM A CARDIAC CATHETERIZATION IN ORDER TO DIAGNOSE OR TREAT THE CHILD&#39;S HEART PROBLEM.</strong></span></p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px\">Paediatric cardiology is concerned with diseases of the heart in the growing and developing individual. As well as expertise in heart disease, paediatric cardiologists also need a thorough grounding in general paediatrics, in order to provide all-round patient care.</span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `ID` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `TYPE` varchar(100) DEFAULT NULL,
  `ACTIVE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`ID`, `username`, `password`, `TYPE`, `ACTIVE`) VALUES
(1, 'admin@pic.edu.pk', '$2y$10$fHFoYLIAMfoe6xAGQfrnnuZfxGoPjvnh.HByCAMAFl/v5rapnyusO', 'ADMIN', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mti_acts`
--

CREATE TABLE `mti_acts` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mti_acts`
--

INSERT INTO `mti_acts` (`id`, `description`, `file`, `active`) VALUES
(1, 'MTI ACT 2015', 'a1.pdf', 1),
(2, 'AMENDMENT-1', 'a2.pdf', 1),
(3, 'AMENDMENT-2', 'a3.pdf', 1),
(4, 'AMENDMENT-3', 'a4.pdf', 1),
(5, 'Rules', 'a5.pdf', 1),
(6, 'By Law Of The Medical Staff PIC 2018', 'Bylaws for the medical staff PIC 2018.pdf', 1),
(7, 'Institute Employee Hand Book Jan 2018', 'Institutional Employees Handbook  Jan 2018 (1).pdf', 1),
(8, 'Regulation For PIC Final', 'REGULATIONS FOR PIC revised.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_image_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_image_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_image_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_image_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_image_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_last_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured_news` int(11) DEFAULT NULL,
  `covid_news` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news_image`, `news_image_description`, `news_details`, `news_image_1`, `news_image_2`, `news_image_3`, `news_image_4`, `news_last_description`, `featured_news`, `covid_news`, `active`, `created_at`, `updated_at`) VALUES
(3, 'staff.jpg', '<p>Health Minister along with NDMA Chairman visited PIC</p>', '<p>Health Minister along with NDMA Chairman visited PIC to establish 200 bed emergency&nbsp;COVID-19 hospital</p>', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, '2020-07-17 10:13:39', '2020-07-17 16:29:36'),
(4, 'covid19.jpg', '<p>Medical equipment provided by NDMA for COVID-19 patients</p>', '<p>Medical equipment provided by NDMA for COVID-19 patients</p>', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, '2020-07-17 10:16:04', '2020-07-17 10:16:04'),
(5, 'Minister Health.jpg', '<p>Minister Health and Finance visted PIC for Inspection of Equipments</p>', '<p>Minister Health and Finance visted PIC for Inspection of Equipments</p>', 'PIC ward.jpg', NULL, NULL, NULL, '<p>PIC wards are setup to operate patients</p>', 0, 0, 1, '2020-07-17 10:24:19', '2020-07-17 10:24:19');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `slider_image` varchar(500) DEFAULT NULL,
  `slider_text` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `slider_image`, `slider_text`) VALUES
(1, '562a71f9dd0895a8388b4593.jpeg', '<p>PESHAWAR INSTITUTE OF CARDIOLOGY <strong><span style=\"color:#2980b9\">WARDS</span></strong></p>'),
(2, 'ward_2.jpg', '<p>Minister Health and Finance Inspection</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tenders`
--

CREATE TABLE `tenders` (
  `id` int(11) NOT NULL,
  `tender_name` varchar(1000) DEFAULT NULL,
  `tender_type` varchar(500) DEFAULT NULL,
  `procurement_entity` varchar(1000) DEFAULT NULL,
  `tender_description` text NOT NULL,
  `tender_eoi` varchar(500) DEFAULT NULL,
  `tender_eoi_2` varchar(500) DEFAULT NULL,
  `sbd` varchar(500) DEFAULT NULL,
  `tender_evaluation` varchar(500) DEFAULT NULL,
  `advertisement_date` varchar(100) DEFAULT NULL,
  `closing_date` varchar(100) DEFAULT NULL,
  `financial_remarks` varchar(1000) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `comments` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tenders`
--

INSERT INTO `tenders` (`id`, `tender_name`, `tender_type`, `procurement_entity`, `tender_description`, `tender_eoi`, `tender_eoi_2`, `sbd`, `tender_evaluation`, `advertisement_date`, `closing_date`, `financial_remarks`, `active`, `status`, `comments`) VALUES
(11, 'SBDs After pre- Bid meeting', NULL, NULL, '<p><a href=\"http://pic.org.pk/tenders/SBDs%20After%20pre-%20Bid%20meeting.pdf\" target=\"_blank\">SBDs After pre- Bid meeting</a></p>', 'SBDs After pre- Bid meeting.pdf', NULL, NULL, NULL, '2020-07-01', '2021-06-30', NULL, 1, 'PENDING', NULL),
(12, 'Final Report of PIC-003', NULL, NULL, '<p><a href=\"http://pic.org.pk/tenders/Final%20Report%20of%20PIC-003.pdf\" target=\"_blank\">Final Report of PIC-003</a></p>', 'Final Report of PIC-003.pdf', NULL, NULL, NULL, '2020-07-01', '2021-07-01', NULL, 1, 'PENDING', NULL),
(13, 'Final BER Tender Angiography-Machine', NULL, NULL, '<p><a href=\"http://pic.org.pk/tenders/Angiography-Machine.pdf\" target=\"_blank\">Final BER Tender Angiography-Machine</a></p>', 'Angiography-Machine.pdf', NULL, NULL, NULL, '2020-07-01', '2021-07-01', NULL, 1, 'PENDING', NULL),
(14, 'Final BER Tender CT-Scan', NULL, NULL, '<p><a href=\"http://pic.org.pk/tenders/CT-Scan.pdf\" target=\"_blank\">Final BER Tender CT-Scan</a></p>', 'CT-Scan.pdf', NULL, NULL, NULL, '2020-07-01', '2021-07-01', NULL, 1, 'PENDING', NULL),
(15, 'Final BER Tender DR-System.pdf', NULL, NULL, '<p><a href=\"http://pic.org.pk/tenders/DR-System.pdf\" target=\"_blank\">Final BER Tender DR-System.pdf</a></p>', 'DR-System.pdf', NULL, NULL, NULL, '2020-07-01', '2021-07-01', NULL, 1, 'PENDING', NULL),
(16, 'Final BER Tender Echo', NULL, NULL, '<p><a href=\"http://pic.org.pk/tenders/Echo.pdf\" target=\"_blank\">Final BER Tender Echo</a></p>', 'Echo.pdf', NULL, NULL, NULL, '2020-07-01', '2021-07-01', NULL, 1, 'PENDING', NULL),
(17, 'Final BER Tender Mobile-Echo', NULL, NULL, '<p><a href=\"http://pic.org.pk/tenders/Mobile-Echo.pdf\" target=\"_blank\">Final BER Tender Mobile-Echo</a></p>', 'Mobile-Echo.pdf', NULL, NULL, NULL, '2020-07-01', '2021-07-01', NULL, 1, 'PENDING', NULL),
(18, 'Final BER Tender Mobile-X-RAY', NULL, NULL, '<p><a href=\"http://pic.org.pk/tenders/Mobile-X-RAY.pdf\" target=\"_blank\">Final BER Tender Mobile-X-RAY</a></p>', 'Mobile-X-RAY.pdf', NULL, NULL, NULL, '2020-07-01', '2021-07-01', NULL, 1, 'PENDING', NULL),
(19, 'SBDs-PIC-004', NULL, 'PHYSIOTHERAPY EQUIPMENT, LANDRY AND CARDIAC SURGERY INSTRUMENTS', '<p>PROCUREMENT OF THE MEDICAL EQUIPMENT, PHYSIOTHERAPY EQUIPMENT, LANDRY AND CARDIAC SURGERY INSTRUMENTS FOR THE YEAR 2020-21 (REF NO PIC-004)</p>', 'SBDs-PIC-004.pdf', NULL, NULL, NULL, '2020-07-01', '2021-07-01', NULL, 1, 'PENDING', NULL),
(20, 'Tender-Notice', NULL, 'PHYSIOTHERAPY EQUIPMENT, LANDRY AND CARDIAC SURGERY INSTRUMENTS', '<h4>PROCUREMENT OF THE MEDICAL EQUIPMENT, PHYSIOTHERAPY EQUIPMENT, LANDRY AND CARDIAC SURGERY INSTRUMENTS FOR THE YEAR 2020-21 (REF NO PIC-004)</h4>', 'Tender-Notice.pdf', NULL, NULL, NULL, '2020-07-01', '2021-07-01', NULL, 1, 'PENDING', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `timeline`
--

CREATE TABLE `timeline` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `created_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutus`
--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bog`
--
ALTER TABLE `bog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_content_section`
--
ALTER TABLE `front_content_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mti_acts`
--
ALTER TABLE `mti_acts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenders`
--
ALTER TABLE `tenders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timeline`
--
ALTER TABLE `timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutus`
--
ALTER TABLE `aboutus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bog`
--
ALTER TABLE `bog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `front_content_section`
--
ALTER TABLE `front_content_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mti_acts`
--
ALTER TABLE `mti_acts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tenders`
--
ALTER TABLE `tenders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `timeline`
--
ALTER TABLE `timeline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
