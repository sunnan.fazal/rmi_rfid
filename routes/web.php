<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get("/" , "BackPortalController@Dashboard");
    Route::get("/tenders" , "BackPortalController@showTenderGrid");
    Route::get("/create-tender" , "BackPortalController@showCreateTender");
    Route::post("/save-tender" , "BackPortalController@saveTender");
    Route::post("/save-edited-tender" , "BackPortalController@saveEditedTender");
    Route::get("/edit-tender/{id?}" , "BackPortalController@editTender");

    Route::get("/careers" , "BackPortalController@showCareersGrid");
    Route::get("/create-advertisement" , "BackPortalController@showCreateCareer");
    Route::post("/save-advertisement" , "BackPortalController@saveAdvertisement");
    Route::get("/edit-career/{id?}" , "BackPortalController@editCareer");
    Route::post("/save-edit-advertisement" , "BackPortalController@saveEditAdvertisement");

    Route::get("/mti-acts" , "BackPortalController@showMtiActs");
    Route::get("/create-mti-act" , "BackPortalController@createMTIActs");
    Route::post("/save-mti-acts" , "BackPortalController@saveMTIActs");

    Route::get("/downloads" , "BackPortalController@showDownloads");
    Route::get("/create-downloads" , "BackPortalController@createDownloads");
    Route::post("/save-downloads" , "BackPortalController@saveDownloads");
    Route::get("/remove-download/{id?}" , "BackPortalController@removeDownload");

    Route::get("/news" , "BackPortalController@showNews");
    Route::get("/create-news" , "BackPortalController@createNews");
    Route::post("/save-news" , "BackPortalController@saveNews");
    Route::get("/edit-news/{id}" , "BackPortalController@editNews");
    Route::post("/save-edit-news" , "BackPortalController@saveEditNews");
    
    Route::get("/front-contents" , "BackPortalController@showFrontContents");
    Route::post("/save-front-content","BackPortalController@saveFrontContent");

    Route::get('/sliders' , 'BackPortalController@showSliders');
    Route::get('/create-slider' , 'BackPortalController@createSlider');
    Route::post('/save-slider' , 'BackPortalController@saveSlider');
    Route::get('/edit-slider/{id?}' , 'BackPortalController@editSlider');
    Route::post('/edit-save-slider' , 'BackPortalController@editSaveSlider');
    
    Route::get('/doctors' , 'BackPortalController@showDoctors');
    Route::get('/create-doctor' , 'BackPortalController@createDoctor');
    Route::post('/save-doctor' , 'BackPortalController@saveDoctor');
    Route::get('/edit-doctor/{id}' , 'BackPortalController@editDoctor');
    Route::post('/edit-save-doctor' , 'BackPortalController@editSaveDoctor');
    
    Route::get('/about-us' , 'BackPortalController@aboutUs');
    Route::post('/save-about-us' , 'BackPortalController@saveAboutUs');

    Route::get('/vision-mission' , 'BackPortalController@visionMission');
    Route::post('/save-vision-mission' , 'BackPortalController@saveVisionMission');
    
    Route::get('/governors' , 'BackPortalController@governorsBoard');
    Route::get('/create-bog' , 'BackPortalController@createGovernorsBoard');
    Route::get('/edit-bog/{id?}' , 'BackPortalController@editBOG');
    Route::post('/save-bog' , 'BackPortalController@saveBOG');
    Route::post('/save-edit-bog' , 'BackPortalController@saveEditBOG');
    
    Route::get('/timeline' , 'BackPortalController@timeline');
    Route::get('/create-timeline' , 'BackPortalController@createTimeline');
    Route::post('/save-timeline' , 'BackPortalController@saveTimeline');
    Route::get('/edit-timeline/{id}' , 'BackPortalController@editTimeline');
    Route::post('/save-edit-timeline' , 'BackPortalController@SaveEditTimeline');
  
    
    
});

