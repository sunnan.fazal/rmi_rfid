$(function () {
    $('.js-sweetalert').on('click', function () {
        var type = $(this).data('type');
        if (type === 'confirm') {
            showConfirmMessage($(this).data('job-title') , $(this).data('job'));
        }
    });
});
function showConfirmMessage(jobTitle,jobId) {
    swal({
        title: jobTitle,
        text: "Are you sure to apply for this job?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#2A426A",
        confirmButtonText: "Yes, Apply!",
        closeOnConfirm: true
    }, function () {
        $.get($("body").attr("data-url")+"/apply-job/"+jobId,function(data){
            if(data == 1)
            {
                $("#success"+jobId).css("display","block");
                $("#success"+jobId).html("Successfully Applied For This Job !");
                setTimeout(function () {
                    $("#success"+jobId).css("display","none");
                }, 3000);
            }
            if(data == -1)
            {
                $("#error"+jobId).css("display","block");
                $("#error"+jobId).html("Already Applied for this Job !");
                setTimeout(function () {
                    $("#error"+jobId).css("display","none");
                }, 3000);
            }
            if(data == -2)
            {
                $("#error"+jobId).css("display","block");
                $("#error"+jobId).html("Please complete your profile !");
                setTimeout(function () {
                    $("#error"+jobId).css("display","none");
                }, 3000);
            }
        });
    });
}