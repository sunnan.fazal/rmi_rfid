@if( $view == "front" )
    <link rel='stylesheet' id='jobhunt-application-deadline-styles-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/jobhunt-application-deadline/assets/css/dealine-style9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='jobhunt-indeed-jobs-styles-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/jobhunt-indeed-jobs/assets/css/jobhunt-indeed-jobs-style9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='jobcareer_font_Montserrat-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3Aregular%2C700&amp;subset=latin&amp;ver=4.8.8' type='text/css' media='all' />
    <link rel='stylesheet' id='jobcareer_font_Raleway-css'  href='http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2Cregular%2C500%2C600%2C700%2C800%2C900&amp;subset=latin&amp;ver=4.8.8' type='text/css' media='all' />
    <link rel='stylesheet' id='jobcareer_iconmoon_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/assets/css/iconmoon9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='cs_bootstrap_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/assets/css/bootstrap9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='jobcareer_style_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/style9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='jobcareer_nav-icon-css'  href='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/assets/css/nav-icon9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='jobcareer_top-menu-css'  href='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/assets/css/top-menu9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='cs_slicknav_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/assets/css/slicknav9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='jobcareer_widgets_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/assets/css/widget9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='jobcareer_prettyPhoto-css'  href='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/assets/css/prettyPhoto9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='cs-woocommerce-css'  href='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/assets/css/cs-woocommerce9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='jobcareer_custom_style_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/assets/css/custom-style6e28.css?ver=c40fb1f64f5fe5e37210498aa326834b") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='cs_iconmoon_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/icomoon/css/iconmoon9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='cs_swiper_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/css/swiper.min9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='cs_jobhunt_plugin_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/css/cs-jobhunt-plugin9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='job-editor-style-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/css/jquery-te-1.4.09537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='cs_datetimepicker_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/css/jquery_datetimepicker9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='cs_bootstrap_slider_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/css/bootstrap-slider9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='cs_chosen_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/css/chosen9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='custom-style-inline-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/css/custom_script9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='jobcareer_responsive_css-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/css/responsive9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/revslider/public/assets/css/settingsf542.css?ver=5.4.8.3") !!}' type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required { visibility: visible; }
    </style>
    <link rel='stylesheet' id='jobhunt-notifications-css-css'  href='{!! url("/resources/assets/updated-website/wp-content/plugins/jobhunt-notifications/assets/css/jobhunt-notifications-frontend9537.css?ver=4.8.8") !!}' type='text/css' media='all' />
    <script type='text/javascript' src='{!! url("/resources/assets/updated-website/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4") !!}'></script>
    <script type='text/javascript' src='{!! url("/resources/assets/updated-website/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1") !!}'></script>
    <script type='text/javascript' src='{!! url("/resources/assets/updated-website/wp-content/plugins/jobhunt-apply-with-facebook/assets/js/apply-fb-style9537.js?ver=4.8.8") !!}'></script>
    <script type='text/javascript' src='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/scripts/jquery-te-1.4.0.min9537.js?ver=4.8.8") !!}'></script>
    <script type='text/javascript' src='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/scripts/modernizr.min9537.js?ver=4.8.8") !!}'></script>
    <script type='text/javascript' src='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/scripts/browser-detect9537.js?ver=4.8.8") !!}'></script>
    <script type='text/javascript' src='{!! url("/resources/assets/updated-website/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.minf542.js?ver=5.4.8.3") !!}'></script>
    <link rel="stylesheet" href="{!! url("/resources/assets/admin/css/sweetalert.css") !!}">
@elseif ( $view == "back" )
    <link rel="stylesheet" href="{!! url("/resources/assets/admin/plugins/bootstrap/css/bootstrap.min.css") !!}">
    <link rel="stylesheet" href="{!! url("/resources/assets/admin/css/main.css") !!}">
    <link rel="stylesheet" href="{!! url("/resources/assets/admin/css/color_skins.css") !!}">
    <link rel="stylesheet" href="{!! url("/resources/assets/admin/css/admin_style.css") !!}">
    <link rel="stylesheet" href="{!! url("/resources/assets/css/kendo.common.min.css") !!}" />
    <link rel="stylesheet" href="{!! url("/resources/assets/css/kendo.blueopal.min.css") !!}" />
    <link rel="stylesheet" href="{!! url("/resources/assets/css/kendo.default.mobile.min.css") !!}" />
    <link rel="stylesheet" href="{!! url("/resources/assets/css/datatable.min.css") !!}" />
@endif
<script src="{!! url("/resources/assets/jquery.min.js") !!} "></script>
<script src="{!! url("/resources/assets/js/kendo.all.min.js") !!} "></script>
<script src="{!! url("/resources/assets/js/datatable.min.js") !!} "></script>

<style>
    .k-grid-toolbar
    {
        background: #1F2A5B !important;
    }
    .k-button > .k-button-icontext
    {
        background: #1F2A5B !important;
        border: #1F2A5B;
    }
    .k-button:hover
    {
        background: #DCEEF6 !important;
        border: #DCEEF6;
        color: black;
    }
    .k-grouping-header
    {
        background: #47C0EF;
        color: white !important;
    }
</style>

