<script type='text/javascript' src='{!! url("/resources/assets/updated-website/wp-content/themes/jobcareer/assets/scripts/bootstrap.min9537.js?ver=4.8.8") !!}'></script>
<script type='text/javascript' src='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/scripts/bootstrap-slider9537.js?ver=4.8.8") !!}'></script>
<script type='text/javascript' src='{!! url("/resources/assets/updated-website/wp-content/plugins/wp-jobhunt/assets/scripts/chosen.jquery9537.js?ver=4.8.8") !!}'></script>
<script src="{!! url("/resources/assets/admin/bundles/mainscripts.bundle.js") !!}"></script>
<script src="{!! url("/resources/assets/js/jszip.js") !!}"></script>
<script src="{!! url("/resources/assets/admin/js/pages/index.js") !!}"></script>
<script>
    $(document).ready(function(){
        //$("input[type='date']").kendoDatePicker();
        $("input[type='date']").kendoDatePicker({
            format: "yyyy-MM-dd",
            dateInput: true
        });

        $(".k-datepicker").css("width" , "100%");
    });
</script>
