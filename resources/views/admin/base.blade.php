<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CMS - Peshawar Institue of Cardiology</title>
    @include("files_header" , ["view" => "back"])
</head>
<body class="theme-blue" data-url="{!! url("/") !!}">
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="{!! url("/resources/assets/images/logo.svg") !!}" width="48" height="48" alt="PIC"></div>
        <p>Please wait...</p>
    </div>
</div>
@include("admin.leftsidebar")
@yield("content")
</body>
@include("files_footer" , ["view" => "back"])
<style>
    a:hover
    {
        background: transparent !important;
    }
</style>
<script>
    $(document).ready(function(){
       $(".page-loader-wrapper").css("display","none");
    });
</script>
</html>