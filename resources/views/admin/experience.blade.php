<div class="header" style="background-color: #f4f4f4;">
    <h2><strong>Experience</strong></h2>
</div>
<hr>
<div class="row">
    <div class="col-sm-10"></div>
    <div class="col-sm-2">
        <a class="addRowExperience"><i class="material-icons" style="color: lightgreen;font-size: 3em;cursor: pointer">add_box</i></a>
    </div>
</div>
<div class="experiences">
    @if(isset($experience))
        @foreach($experience as $exp)
            <div class="experienceROW">
                <div class="row clearfix experienceROW">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="" style="font-size: 0.9em;">Organization</label>
                            <input type="text" name="organization[]" class="form-control square" placeholder="Organization" maxlength="255" value="{!! $exp->ORGANIZATION !!}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="" style="font-size: 0.9em;">Designation / Position</label>
                            <input type="text" name="designation[]" class="form-control square" placeholder="Designation / Position" maxlength="200" value="{!! $exp->DESIGNATION !!}" >
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="" style="font-size: 0.9em;">Speciality</label>
                            <input type="text" name="speciality[]" class="form-control square" placeholder="Speciality" maxlength="200" value="{!! $exp->SPECIALITY !!}">
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2" >
                        <a data-id="{!! $exp->ID !!}" onclick="removeRowExperience(this)"><i class="material-icons" style="color: lightcoral;font-size: 3em;margin-top: 0.5em;cursor: pointer">indeterminate_check_box</i></a>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="" style="font-size: 0.9em;">Start Date</label>
                            <input type="date" name="startDate[]" class="form-control square" placeholder="" value="{!! $exp->START_DATE !!}" >
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="" style="font-size: 0.9em;">End Date</label>
                            <input type="date" name="endDate[]" class="form-control square" placeholder="" value="{!! $exp->END_DATE !!}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="" style="font-size: 0.9em;">Last Salary</label>
                            <input type="number" name="lastSalary[]" class="form-control square" placeholder="Last Salary" max="1000000" min="0" value="{!! $exp->LAST_SALARY !!}" >
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="" style="font-size: 0.9em;">Leaving Reason</label>
                            <input type="text" name="leavingReason[]" class="form-control square" placeholder="Leaving Reason" maxlength="255" value="{!! $exp->LEAVING_REASON !!}">
                        </div>
                    </div>
                </div>
                <br>
            </div>
            <hr>
        @endforeach
    @endif
</div>
<script type="text/html" id="experienceROWS">
    <div class="experienceROW">
        <div class="row clearfix">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="" style="font-size: 0.9em;">Organization</label>
                    <input type="text" name="organization[]" class="form-control square" placeholder="Organization" maxlength="200" >
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="" style="font-size: 0.9em;">Designation / Position</label>
                    <input type="text" name="designation[]" class="form-control square" placeholder="Designation / Position" maxlength="200" >
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="" style="font-size: 0.9em;">Speciality</label>
                    <input type="text" name="speciality[]" class="form-control square" placeholder="Speciality" maxlength="100"  >
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-2" >
                <a data-id="" onclick="removeRowExperience(this)"><i class="material-icons" style="color: lightcoral;font-size: 3em;margin-top: 0.5em;cursor: pointer">indeterminate_check_box</i></a>
            </div>
        </div>
            <div class="row clearfix">
            <div class="col-sm-2">
                <div class="form-group">
                    <label for="" style="font-size: 0.9em;">Start Date</label>
                    <input type="date" name="startDate[]" class="form-control square" placeholder="" >
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label for="" style="font-size: 0.9em;">End Date</label>
                    <input type="date" name="endDate[]" class="form-control square" placeholder="">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label for="" style="font-size: 0.9em;">Last Salary</label>
                    <input type="number" name="lastSalary[]" class="form-control square" placeholder="Last Salary" max="1000000" min="0" >
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="" style="font-size: 0.9em;">Leaving Reason</label>
                    <input type="text" name="leavingReason[]" class="form-control square" placeholder="Leaving Reason" maxlength="255">
                </div>
            </div>
            </div>
    <br>
    <hr>
</div>

</script>
<script>
    $(document).ready(function () {
        $(".experiences").append($("#experienceROWS").html());
        $(".addRowExperience").on("click" ,function(){
            $(".experiences").append($("#experienceROWS").html());
        });
    });
    function removeRowExperience(val)
    {
        if($(val).attr("data-id") != "")
        {
            $.get($("body").attr("data-url")+"/remove-experience/"+$(val).attr("data-id"), function(data, status){
                $(val).closest("div.experienceROW").remove();
            });
        }
        $(val).closest("div.experienceROW").remove();
    }
</script>

