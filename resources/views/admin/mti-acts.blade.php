@extends('admin.base')
@section("content")
    <style>
        body{
            overflow: hidden;
        }
    </style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>All MTI Acts Rules & Regulations
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="body">
                            <a href="{{ url('/create-mti-act') }}" class="btn btn-primary" style="float: right">Create Rules & Regulations</a>
                            <br>
                            <br>
                            <br>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Description</th>
                                    <th>Attachment</th>
                                    <th>Active</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($mti_act as $act)
                                        <tr>
                                            <td>{{ $act->id }}</td>
                                            <td>{{ $act->description }}</td>
                                            <td><a target="_blank"  href="{!! url('resources/mti_acts/'.$act->file) !!}">Document</a></td>
                                            <td><input type="checkbox" {!! $act->active == 1 ? 'checked' : '' !!}></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            $('.table').DataTable();
        });
    </script>

@endsection