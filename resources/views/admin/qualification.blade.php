<div class="header" style="background-color: #f4f4f4;">
    <h2><strong>Qualification</strong></h2>
</div>
<hr>
<div class="row">
    <div class="col-sm-10"></div>
    <div class="col-sm-2">
        <a class="addRowQualification"><i class="material-icons" style="color: lightgreen;font-size: 3em;cursor: pointer;">add_box</i></a>
    </div>
</div>
<div class="qualifications">
    @if(isset($qualification))
        @foreach($qualification as $qualify)
            <div class="row clearfix qualificationROW">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="" style="font-size: 0.9em;">Degree</label>
                        <input type="text" name="degree[]" id="" class="form-control square" value="{!! $qualify->DEGREE !!}">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="" style="font-size: 0.9em;">Institute</label>
                        <input type="text" class="form-control square" name="institution[]" placeholder="Institute Name" value="{!! $qualify->INSTITUTE_NAME !!}">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="" style="font-size: 0.9em;">Passing Year</label>
                        <select name="passing[]" id="" class="form-control square">
                            <option {!! ($qualify->PASSING_YEAR == "2000") ? "selected" : "" !!} value="2000">2000</option>
                            <option {!! ($qualify->PASSING_YEAR == "2001") ? "selected" : "" !!} value="2001">2001</option>
                            <option {!! ($qualify->PASSING_YEAR == "2002") ? "selected" : "" !!} value="2002">2002</option>
                            <option {!! ($qualify->PASSING_YEAR == "2003") ? "selected" : "" !!} value="2003">2003</option>
                            <option {!! ($qualify->PASSING_YEAR == "2004") ? "selected" : "" !!} value="2004">2004</option>
                            <option {!! ($qualify->PASSING_YEAR == "2005") ? "selected" : "" !!} value="2005">2005</option>
                            <option {!! ($qualify->PASSING_YEAR == "2006") ? "selected" : "" !!} value="2006">2006</option>
                            <option {!! ($qualify->PASSING_YEAR == "2007") ? "selected" : "" !!} value="2007">2007</option>
                            <option {!! ($qualify->PASSING_YEAR == "2008") ? "selected" : "" !!} value="2008">2008</option>
                            <option {!! ($qualify->PASSING_YEAR == "2009") ? "selected" : "" !!} value="2009">2009</option>
                            <option {!! ($qualify->PASSING_YEAR == "2010") ? "selected" : "" !!} value="2010">2010</option>
                            <option {!! ($qualify->PASSING_YEAR == "2011") ? "selected" : "" !!} value="2011">2011</option>
                            <option {!! ($qualify->PASSING_YEAR == "2012") ? "selected" : "" !!} value="2012">2012</option>
                            <option {!! ($qualify->PASSING_YEAR == "2013") ? "selected" : "" !!} value="2013">2013</option>
                            <option {!! ($qualify->PASSING_YEAR == "2014") ? "selected" : "" !!} value="2014">2014</option>
                            <option {!! ($qualify->PASSING_YEAR == "2015") ? "selected" : "" !!} value="2015">2015</option>
                            <option {!! ($qualify->PASSING_YEAR == "2016") ? "selected" : "" !!} value="2016">2016</option>
                            <option {!! ($qualify->PASSING_YEAR == "2017") ? "selected" : "" !!} value="2017">2017</option>
                            <option {!! ($qualify->PASSING_YEAR == "2018") ? "selected" : "" !!} value="2018">2018</option>
                            <option {!! ($qualify->PASSING_YEAR == "2019") ? "selected" : "" !!} value="2019">2019</option>
                            <option {!! ($qualify->PASSING_YEAR == "2020") ? "selected" : "" !!} value="2020">2020</option>
                            <option {!! ($qualify->PASSING_YEAR == "2021") ? "selected" : "" !!} value="2021">2021</option>
                            <option {!! ($qualify->PASSING_YEAR == "2022") ? "selected" : "" !!} value="2022">2022</option>
                            <option {!! ($qualify->PASSING_YEAR == "2023") ? "selected" : "" !!} value="2023">2023</option>
                            <option {!! ($qualify->PASSING_YEAR == "2024") ? "selected" : "" !!} value="2024">2024</option>
                            <option {!! ($qualify->PASSING_YEAR == "2025") ? "selected" : "" !!} value="2025">2025</option>
                            <option {!! ($qualify->PASSING_YEAR == "2026") ? "selected" : "" !!} value="2026">2026</option>
                            <option {!! ($qualify->PASSING_YEAR == "2027") ? "selected" : "" !!} value="2027">2027</option>
                            <option {!! ($qualify->PASSING_YEAR == "2028") ? "selected" : "" !!} value="2028">2028</option>
                            <option {!! ($qualify->PASSING_YEAR == "2029") ? "selected" : "" !!} value="2029">2029</option>
                            <option {!! ($qualify->PASSING_YEAR == "2030") ? "selected" : "" !!} value="2030">2030</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="" style="font-size: 0.9em;">Grade</label>
                        <input type="text" class="form-control square" name="grade[]" placeholder="Grade" value="{!! $qualify->GRADE !!}">
                    </div>
                </div>
                <div class="col-sm-2">
                    <a data-id="{!! $qualify->ID !!}" onclick="removeRowQualification(this)"><i class="material-icons" style="color: lightcoral;font-size: 3em;margin-top: 0.55em;cursor: pointer;">indeterminate_check_box</i></a>
                </div>
                <br>
            </div>
        @endforeach
    @endif
</div>
<script type="text/html" id="qualificationROWS">
<div class="row clearfix qualificationROW">
    <div class="col-sm-3">
        <div class="form-group">
            <label for="" style="font-size: 0.9em;">Degree</label>
            <input type="text" name="degree[]" id="" class="form-control square">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="" style="font-size: 0.9em;">Institute</label>
            <input type="text" class="form-control square" name="institution[]" placeholder="Institute Name">
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label for="" style="font-size: 0.9em;">Passing Year</label>
            <select name="passing[]" id="" class="form-control square">
                <option value="2000">2000</option>
                <option value="2001">2001</option>
                <option value="2002">2002</option>
                <option value="2003">2003</option>
                <option value="2004">2004</option>
                <option value="2005">2005</option>
                <option value="2006">2006</option>
                <option value="2007">2007</option>
                <option value="2008">2008</option>
                <option value="2009">2009</option>
                <option value="2010">2010</option>
                <option value="2011">2011</option>
                <option value="2012">2012</option>
                <option value="2013">2013</option>
                <option value="2014">2014</option>
                <option value="2015">2015</option>
                <option value="2016">2016</option>
                <option value="2017">2017</option>
                <option value="2018">2018</option>
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
                <option value="2026">2026</option>
                <option value="2027">2027</option>
                <option value="2028">2028</option>
                <option value="2029">2029</option>
                <option value="2030">2030</option>
            </select>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label for="" style="font-size: 0.9em;">Grade/GPA</label>
            <input type="text" class="form-control square" name="grade[]" placeholder="Grade">
        </div>
    </div>
    <div class="col-sm-2">
        <a data-id="" onclick="removeRowQualification(this)"><i class="material-icons" style="color: lightcoral;font-size: 3em;margin-top: 0.55em;cursor: pointer;">indeterminate_check_box</i></a>
    </div>
    <br>
</div>
</script>
<script>
    $(document).ready(function () {
        $(".qualifications").append($("#qualificationROWS").html());
        $(".addRowQualification").on("click" ,function(){
            $(".qualifications").append($("#qualificationROWS").html());
        });
    });
    function removeRowQualification(val)
    {
        if($(val).attr("data-id") != "")
        {
            $.get($("body").attr("data-url")+"/remove-qualification/"+$(val).attr("data-id"), function(data, status){
                console.log(data);
                $(val).closest("div.qualificationROW").remove();
            });
        }
        $(val).closest("div.qualificationROW").remove();
    }
</script>
