@extends('admin.base')
@section("content")

    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Departments
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong>Basic</strong> Information</h2>
                        </div>
                        @if(! isset($department))
                            <div class="body">
                                <form action="{!! url("save-department") !!}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <label for="">Enter Department<span style="color:red">*</span></label>
                                                <input type="text" class="form-control square" name="departmentName" max="255" placeholder="Department Name" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton">Save Department</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @elseif (isset($department))
                            <div class="body">
                                <form action="{!! url("edit-save-department") !!}" method="post">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="departmentId" value="{!! $department->ID !!}">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <label for="">Enter Department<span style="color:red">*</span></label>
                                                <input type="text" class="form-control square" name="departmentName" max="255" value="{!! $department->DEPARTMENT_NAME !!}" placeholder="City Name" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton">Update Department</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection