@extends('admin.base')
@section("content")

    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Cities
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong>Basic</strong> Information</h2>
                        </div>
                        @if(! isset($city))
                            <div class="body">
                                <form action="{!! url("save-city") !!}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <label for="">Enter City Name <span style="color:red">*</span></label>
                                                <input type="text" class="form-control square" name="city" max="255" placeholder="City Name" required>
                                                @if($errors->any())
                                                    <p style="color: red;">
                                                        City <b>{!! old('city') !!}</b> already Exists !
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton">Save City</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @elseif (isset($city))
                            <div class="body">
                                <form action="{!! url("edit-save-city") !!}" method="post">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="cityID" value="{!! $city->ID !!}">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <label for="">Enter City Name <span style="color:red">*</span></label>
                                                <input type="text" class="form-control square" name="city" max="255" value="{!! $city->CITY_NAME !!}" placeholder="City Name" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton">Update City</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection