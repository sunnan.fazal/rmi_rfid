@extends('admin.base')
@section("content")
    
<style type="text/css">
    .content_pic_1 {
        width: 450px;
        height:  250px;
    }
    .content_pic_2{
        width: 450px;
        height:  250px;
    } 
    .content_pic_3 {
        width: 450px;
        height:  250px;
    }   
</style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Create Front Contents
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <form action="{!! url("/save-front-content") !!}" method="post" enctype="multipart/form-data">
                            <div class="header">
                                <h2><strong>Top</strong> Section</h2>

                            </div>
                            <div class="body">
                                <input type="hidden" name="level" value="1" />
                                {!! csrf_field() !!}
                                
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($first_content) > 0)
                                                @if( $first_content->first()->content_pic != NULL )
                                                    <img class="pull-left flip mr-15 thumbnail content_pic_1"  src="{{ url('/resources/contents/' . $first_content->first()->content_pic) }}" alt="">
                                                @else
                                                    <img class="pull-left flip mr-15 thumbnail content_pic_1"  src="http://placehold.it/430x240" alt="">
                                                @endif
                                            @else    
                                                <img class="pull-left flip mr-15 thumbnail content_pic_1"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="pic" class="form-control pic_1">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <textarea class="form-control ckeditor" name="contentDescription">{{ (count($first_content) > 0) ? $first_content->first()->content_description : '' }}</textarea>
                                    </div>
                                </div>
                           
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton"><b>Submit Content</b></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <form action="{!! url("/save-front-content") !!}" method="post" enctype="multipart/form-data">
                            <div class="header">
                                <h2><strong>Second</strong> Section</h2>

                            </div>
                            <div class="body">
                                <input type="hidden" name="level" value="2" />
                                {!! csrf_field() !!}
                                
                                <div class="row clearfix">
                                    
                                    <div class="col-sm-8">
                                        <textarea class="form-control ckeditor" name="contentDescription">{{ (count($second_content) > 0) ? $second_content->first()->content_description : '' }}</textarea>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            
                                            @if(count($second_content) > 0)
                                                @if( $second_content->first()->content_pic != NULL )
                                                    <img class="pull-left flip mr-15 thumbnail content_pic_2"  src="{{ url('/resources/contents/' . $second_content->first()->content_pic) }}" alt="">
                                                @else
                                                    <img class="pull-left flip mr-15 thumbnail content_pic_2"  src="http://placehold.it/430x240" alt="">
                                                @endif
                                            @else    
                                                <img class="pull-left flip mr-15 thumbnail content_pic_2"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="pic" class="form-control pic_2">
                                        </div>
                                    </div>
                                    
                                </div>
                           
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton"><b>Submit Content</b></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <form action="{!! url("/save-front-content") !!}" method="post" enctype="multipart/form-data">
                            <div class="header">
                                <h2><strong>Third</strong> Section</h2>

                            </div>
                            <div class="body">
                                <input type="hidden" name="level" value="3" />
                                {!! csrf_field() !!}
                                
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($third_content) > 0)
                                                @if( $third_content->first()->content_pic != NULL )
                                                    <img class="pull-left flip mr-15 thumbnail content_pic_3"  src="{{ url('/resources/contents/' . $third_content->first()->content_pic) }}" alt="">
                                                @else
                                                    <img class="pull-left flip mr-15 thumbnail content_pic_3"  src="http://placehold.it/430x240" alt="">
                                                @endif
                                            @else    
                                                <img class="pull-left flip mr-15 thumbnail content_pic_3"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="pic" class="form-control pic_3">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <textarea class="form-control ckeditor" name="contentDescription">{{ (count($third_content) > 0) ? $third_content->first()->content_description : '' }}</textarea>
                                    </div>
                                </div>
                           
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton"><b>Submit Content</b></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
         $(function () {
            $(".pic_1").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_2").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_1;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_3").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_2;
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });

        function imageIsLoaded(e) {
            $('.content_pic_1').attr('src', e.target.result);
        };
        function imageIsLoaded_1(e) {
            $('.content_pic_2').attr('src', e.target.result);
        };
        function imageIsLoaded_2(e) {
            $('.content_pic_3').attr('src', e.target.result);
        };
        
    </script>
    <script src="{!! url("/resources/assets/ckeditor/ckeditor.js") !!}"></script>
    <script>
        CKEDITOR.replace('ckeditor');
    </script>
@endsection