@extends('admin.base')
@section("content")
    <style>
        .bgColor {
            max-width: 440px;
            height: 400px;
            background-color: #c3e8cb;
            padding: 30px;
            border-radius: 4px;
            text-align: center;
        }
        #targetOuter{
            position:relative;
            text-align: center;
            background-color: #f4f4f4;
            margin: 0px auto;
            width: 150px;
            height: 150px;
            border-radius: 4px;
        }
        .btnSubmit {
            background-color: #565656;
            border-radius: 4px;
            padding: 10px;
            border: #333 1px solid;
            color: #FFFFFF;
            width: 200px;
            cursor:pointer;
        }
        .inputFile {
            padding: 5px 0px;
            margin-top:8px;
            background-color: #FFFFFF;
            width: 100%;
            height: 100%;
            overflow: hidden;
            opacity: 0;
            cursor:pointer;
        }
        .icon-choose-image {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -24px;
            margin-left: -24px;
            width: 48px;
            height: 48px;
        }
        .upload-preview {border-radius:4px;}
        #body-overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: absolute;left: 0;top: 0;width: 100%;height: 100%;display: none;}
        #body-overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
        .image-upload img
        {
            width: 50px;
            cursor: pointer;
        }

    </style>
    <section class="content home" style="margin: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Dashboard
                        <small>Welcome to User Dashboard</small>
                    </h2>
                </div>

                <div class="col-sm-2" style="text-align: right;">
                    <a href="{!! url("/") !!}"><h6 style="color: white;margin-top: 1em;margin-left: 1em;">Search Jobs</h6></a>
                </div>
                <div class="col-sm-2" style="text-align: center;">
                    <a href="{!! url("/dashboard") !!}"><h6 style="color: white;margin-top: 1em;margin-left: 1em;">Dashboard</h6></a>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;margin-top: 1em;margin-left: 1em;font-weight: bold"><i class="zmdi zmdi-power"></i></h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <form action="{!! url("/save-account") !!}" method="post" enctype="multipart/form-data">
                                <div class="header" style="background-color: #f4f4f4;">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <h2><strong>Basic</strong> Information</h2>
                                        </div>
                                        <div class="col-sm-5" style="text-align: right">
                                            <button type="submit" class=" btn btn-round square rmiDarkButton" style="margin-bottom: -14px;margin-top: -18px;">Update Profile</button>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="container alertContainer">
                                    @if(session("msg"))
                                        <div class="row " >
                                            <div class="col-sm-12">
                                                <div class="alert alert-{!! session("type") !!}" style="background-color: #DFF0D8 !important;color: #3c763d">{!! session("msg") !!}</div>
                                            </div>
                                        </div>
                                        <br>
                                    @endif
                                </div>
                                {!! csrf_field() !!}
                                <div class="row clearfix">
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">First Name</label>
                                                    <input type="text" class="form-control square" name="firstName" placeholder="First Name" required maxlength="50" value="{!! ($candidateProfile != NULL) ? $candidateProfile->FIRST_NAME : '' !!}">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Last Name</label>
                                                    <input type="text" class="form-control square" name="lastName" placeholder="Last Name" required maxlength="100" value="{!! ($candidateProfile != NULL) ? $candidateProfile->LAST_NAME : '' !!}">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Father / Spouse Name</label>
                                                    <input type="text" class="form-control square" name="fatherSpouseName" placeholder="Father/Spouse Name" required maxlength="100" value="{!! ($candidateProfile != NULL) ? $candidateProfile->FATHER_SPOUSE : '' !!}">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Gender</label>
                                                    <select name="gender" class="form-control square" required>
                                                        <option value="">--Select Gender--</option>
                                                        <option value="Male" {!! ($candidateProfile != NULL) ? ($candidateProfile->GENDER == "Male" ? 'selected' : '') : '' !!}>Male</option>
                                                        <option value="Female" {!! ($candidateProfile != NULL) ? ($candidateProfile->GENDER == "Female" ? 'selected' : '') : '' !!}>Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Date of Birth</label>
                                                    <input type="date" class="form-control square" name="dob" required maxlength="20" value="{!! ($candidateProfile != NULL) ? $candidateProfile->DOB : '' !!}">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Martial Status</label>
                                                    <select name="maritalStatus" class="form-control square">
                                                        <option value="">--Select Marital Status--</option>
                                                        <option value="Single" {!! ($candidateProfile != NULL) ? ($candidateProfile->MARITAL_STATUS == "Single" ? 'selected' : '') : '' !!}>Single</option>
                                                        <option value="Married" {!! ($candidateProfile != NULL) ? ($candidateProfile->MARITAL_STATUS == "Married" ? 'selected' : '') : '' !!}>Married</option>
                                                        <option value="Other"  {!! ($candidateProfile != NULL) ? ($candidateProfile->MARITAL_STATUS == "Other" ? 'selected' : '') : '' !!}>Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Religion</label>
                                                    <input type="text" class="form-control square" name="religion" placeholder="Religion" maxlength="50" value="{!! ($candidateProfile != NULL) ? $candidateProfile->RELIGION : '' !!}">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Blood Group</label>
                                                    <select name="bloodGroup" class="form-control square">
                                                        <option value="">--Select Blood Group--</option>
                                                        <option {!! ($candidateProfile != NULL) ? ($candidateProfile->BLOOD_GROUP == "A+" ? 'selected' : '') : '' !!} value="A+">A+</option>
                                                        <option {!! ($candidateProfile != NULL) ? ($candidateProfile->BLOOD_GROUP == "A-" ? 'selected' : '') : '' !!} value="A-">A-</option>
                                                        <option {!! ($candidateProfile != NULL) ? ($candidateProfile->BLOOD_GROUP == "B+" ? 'selected' : '') : '' !!} value="B+">B+</option>
                                                        <option {!! ($candidateProfile != NULL) ? ($candidateProfile->BLOOD_GROUP == "B-" ? 'selected' : '') : '' !!} value="B-">B-</option>
                                                        <option {!! ($candidateProfile != NULL) ? ($candidateProfile->BLOOD_GROUP == "O+" ? 'selected' : '') : '' !!} value="O+">O+</option>
                                                        <option {!! ($candidateProfile != NULL) ? ($candidateProfile->BLOOD_GROUP == "O-" ? 'selected' : '') : '' !!} value="O-">O-</option>
                                                        <option {!! ($candidateProfile != NULL) ? ($candidateProfile->BLOOD_GROUP == "AB+" ? 'selected' : '') : '' !!} value="AB+">AB+</option>
                                                        <option {!! ($candidateProfile != NULL) ? ($candidateProfile->BLOOD_GROUP == "AB-" ? 'selected' : '') : '' !!} value="AB-">AB-</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">CNIC / Passport No.</label>
                                                    <input type="text" name="cnic" class="form-control square" placeholder="CNIC / Passport" maxlength="20" value="{!! ($candidateProfile != NULL) ? $candidateProfile->CNIC : '' !!}" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Nationality</label>
                                                    <input type="text" class="form-control square" name="nationality" placeholder="Nationality" required maxlength="50" value="{!! ($candidateProfile != NULL) ? $candidateProfile->NATIONALITY : '' !!}">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Driving License Type</label>
                                                    <input type="text" class="form-control square" name="licenseType" placeholder="Driving License" value="{!! ($candidateProfile != NULL) ? $candidateProfile->DRIVING_LICENSE : '' !!}">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Driving License Number</label>
                                                    <input type="text" class="form-control square" name="licenseNumber" placeholder="Driving License Number" value="{!! ($candidateProfile != NULL) ? $candidateProfile->DRIVING_LICENSE_NUMBER : '' !!}">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-4" style="text-align: center; ">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <div class="avatar-upload" style="margin: 0px auto">
                                                        <div class="avatar-edit">
                                                            <input type='file' name="accountPic"  id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                            <label for="imageUpload">
                                                                <i class="zmdi zmdi-edit" style="margin-top: 6px;color: grey;"></i>
                                                            </label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url({!! ($candidateProfile->CANDIDATE_PIC != NULL || $candidateProfile->CANDIDATE_PIC != "") ? url("/storage/app/pics/".$candidateProfile->CANDIDATE_PIC) : url("/resources/assets/icon-person.png") !!});">
                                                            </div>
                                                        </div>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <br><br>
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-8" style="padding-top: 1.8em;">
                                                {{--@if($candidateProfile->CANDIDATE_CV != NULL || $candidateProfile->CANDIDATE_CV != "")
                                                    <a target="_blank" class="btn btn-primary" href="{!! url("/storage/app/cv/".$candidateProfile->CANDIDATE_CV) !!}">Download Resume</a>
                                                    <a href="{!! url("/remove-cv") !!}" title="Delete Resume"><div class="demo-google-material-icon"> <i class="material-icons">cancel</i></div></a>
                                                @endif
                                                @if($candidateProfile->CANDIDATE_CV == NULL || $candidateProfile->CANDIDATE_CV == "")
                                                    <div class="image-upload">
                                                        <label for="">Upload Resume</label>
                                                        <label for="cvUpload">
                                                            <img src="{!! url("/resources/assets/images/cloud.png") !!}"/>
                                                        </label>
                                                        <p style="color: red;font-size: 10px;">File size should not exceed 2MB</p>
                                                        <b><label id="cvLblSize" style="color: red;"/></b>
                                                        <input id="cvUpload" type="file" name="cv" accept=".pdf,.doc,.docx" style="display: none"/>
                                                    </div>
                                                @endif--}}

                                                <div class="input-group">
                                                    <label class="input-group-btn">
                                                        <span class="btn btn-primary" style="margin-top: -4px;border: 2px solid #4285f4!important;background-color: transparent!important;color: #4285f4!important;">
                                                        Upload Resume <i class="zmdi zmdi-cloud-upload"></i> <input type="file" accept=".pdf,.doc,.docx" name="cv" style="display: none;" multiple="">
                                                        </span>
                                                    </label>
                                                    <input type="text" class="form-control square" readonly="">
                                                    @if($candidateProfile->CANDIDATE_CV != NULL && $candidateProfile->CANDIDATE_CV != "")
                                                        <a target="_blank" href="{!! url("/storage/app/cv/".$candidateProfile->CANDIDATE_CV) !!}"><i class="zmdi zmdi-eye" style="color: #3eacff;font-size: 2em;margin: 6px;"></i></a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="header" style="background-color: #f4f4f4;">
                                    <h2><strong>Contact</strong> Details</h2>
                                </div>
                                <hr>

                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Address</label>
                                            <input type="text" class="form-control square" maxlength="200" name="currentAddress" placeholder="Postal Address" required value="{!! ($candidateProfile != NULL) ? $candidateProfile->ADDRESS : '' !!}">
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">City</label>
                                            <select name="city" id="" class="form-control square">
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Athmuqam') ? 'selected' : '' : '' ) : '' !!} value="Athmuqam">Athmuqam</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Bagh') ? 'selected' : '' : '' ) : '' !!} value="Bagh">Bagh</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Kotli') ? 'selected' : '' : '' ) : '' !!} value="Kotli">Kotli</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'New Mirpur') ? 'selected' : '' : '' ) : '' !!} value="New Mirpur">New Mirpur</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Rawala Kot') ? 'selected' : '' : '' ) : '' !!} value="Rawala Kot">Rawala Kot</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Awaran') ? 'selected' : '' : '' ) : '' !!} value="Awaran">Awaran</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Barkhan') ? 'selected' : '' : '' ) : '' !!} value="Barkhan">Barkhan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Chaman') ? 'selected' : '' : '' ) : '' !!} value="Chaman">Chaman</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Dalbandin') ? 'selected' : '' : '' ) : '' !!} value="Dalbandin">Dalbandin</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Dera Allahyar') ? 'selected' : '' : '' ) : '' !!} value="Dera Allahyar">Dera Allahyar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Dera Bugti') ? 'selected' : '' : '' ) : '' !!} value="Dera Bugti">Dera Bugti</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Dera Murad Jamali') ? 'selected' : '' : '' ) : '' !!} value="Dera Murad Jamali">Dera Murad Jamali</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Gandava') ? 'selected' : '' : '' ) : '' !!} value="Gandava">Gandava</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Gwadar') ? 'selected' : '' : '' ) : '' !!} value="Gwadar">Gwadar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Kalat') ? 'selected' : '' : '' ) : '' !!} value="Kalat">Kalat</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Kharan') ? 'selected' : '' : '' ) : '' !!} value="Kharan">Kharan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Khuzdar') ? 'selected' : '' : '' ) : '' !!} value="Khuzdar">Khuzdar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Kohlu') ? 'selected' : '' : '' ) : '' !!} value="Kohlu">Kohlu</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Loralai') ? 'selected' : '' : '' ) : '' !!} value="Loralai">Loralai</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Mastung') ? 'selected' : '' : '' ) : '' !!} value="Mastung">Mastung</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Musa Khel Bazar') ? 'selected' : '' : '' ) : '' !!} value="Musa Khel Bazar">Mūsa Khel Bazar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Panjgur') ? 'selected' : '' : '' ) : '' !!} value="Panjgur">Panjgūr</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Pishin') ? 'selected' : '' : '' ) : '' !!} value="Pishin">Pishin</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Qila Abdullah') ? 'selected' : '' : '' ) : '' !!} value="Qila Abdullah">Qila Abdullah</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Qila Saifullah') ? 'selected' : '' : '' ) : '' !!} value="Qila Saifullah">Qila Saifullah</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Quetta') ? 'selected' : '' : '' ) : '' !!} value="Quetta">Quetta</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Sibi') ? 'selected' : '' : '' ) : '' !!} value="Sibi">Sibi</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Turbat') ? 'selected' : '' : '' ) : '' !!} value="Turbat">Turbat</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Uthal') ? 'selected' : '' : '' ) : '' !!} value="Uthal">Uthal</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Zhob') ? 'selected' : '' : '' ) : '' !!} value="Zhob">Zhob</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Ziarat') ? 'selected' : '' : '' ) : '' !!} value="Ziarat">Ziarat</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Aliabad') ? 'selected' : '' : '' ) : '' !!} value="Aliabad">Aliabad</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Chilas') ? 'selected' : '' : '' ) : '' !!} value="Chilas">Chilas</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Eidgah') ? 'selected' : '' : '' ) : '' !!} value="Eidgah">Eidgah</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Gakuch') ? 'selected' : '' : '' ) : '' !!} value="Gakuch">Gakuch</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Gilgit') ? 'selected' : '' : '' ) : '' !!} value="Gilgit">Gilgit</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Islamabad') ? 'selected' : '' : '' ) : '' !!} value="Islamabad">Islamabad</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Abbottabad') ? 'selected' : '' : '' ) : '' !!} value="Abbottabad">Abbottabad</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Alpurai') ? 'selected' : '' : '' ) : '' !!} value="Alpurai">Alpūrai</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Bannu') ? 'selected' : '' : '' ) : '' !!} value="Bannu">Bannu</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Bardar') ? 'selected' : '' : '' ) : '' !!} value="Bardar">Bardar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Batgram') ? 'selected' : '' : '' ) : '' !!} value="Batgram">Batgram</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Charsadda') ? 'selected' : '' : '' ) : '' !!} value="Charsadda">Charsadda</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Chitral') ? 'selected' : '' : '' ) : '' !!} value="Chitral">Chitral</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Daggar') ? 'selected' : '' : '' ) : '' !!} value="Daggar">Daggar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Dasu') ? 'selected' : '' : '' ) : '' !!} value="Dasu">Dasu</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Dera Ismail Khan') ? 'selected' : '' : '' ) : '' !!} value="Dera Ismail Khan">Dera Ismail Khan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Hangu') ? 'selected' : '' : '' ) : '' !!} value="Hangu">Hangu</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Haripur') ? 'selected' : '' : '' ) : '' !!} value="Haripur">Haripur</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Karak') ? 'selected' : '' : '' ) : '' !!} value="Karak">Karak</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Kohat') ? 'selected' : '' : '' ) : '' !!} value="Kohat">Kohat</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Lakki Marwat') ? 'selected' : '' : '' ) : '' !!} value="Lakki Marwat">Lakki Marwat</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Malakand') ? 'selected' : '' : '' ) : '' !!} value="Malakand">Malakand</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Mansehra') ? 'selected' : '' : '' ) : '' !!} value="Mansehra">Mansehra</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Mardan') ? 'selected' : '' : '' ) : '' !!} value="Mardan">Mardan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Mehra') ? 'selected' : '' : '' ) : '' !!} value="Mehra">Mehra</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Nowshera') ? 'selected' : '' : '' ) : '' !!} value="Nowshera">Nowshera</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Parachinar') ? 'selected' : '' : '' ) : '' !!} value="Parachinar">Parachinar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Peshawar') ? 'selected' : '' : '' ) : '' !!} value="Peshawar">Peshawar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Saidu Sharif') ? 'selected' : '' : '' ) : '' !!} value="Saidu Sharif">Saidu Sharif</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Serai') ? 'selected' : '' : '' ) : '' !!} value="Serai">Serai</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Swabi') ? 'selected' : '' : '' ) : '' !!} value="Swabi">Swabi</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Tank') ? 'selected' : '' : '' ) : '' !!} value="Tank">Tank</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Timargara') ? 'selected' : '' : '' ) : '' !!} value="Timargara">Timargara</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Upper Dir') ? 'selected' : '' : '' ) : '' !!} value="Upper Dir">Upper Dir</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Attock') ? 'selected' : '' : '' ) : '' !!} value="Attock">Attock</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Bahawalnagar') ? 'selected' : '' : '' ) : '' !!} value="Bahawalnagar">Bahawalnagar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Bahawalpur') ? 'selected' : '' : '' ) : '' !!} value="Bahawalpur">Bahawalpur</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Bhakkar') ? 'selected' : '' : '' ) : '' !!} value="Bhakkar">Bhakkar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Chakwal') ? 'selected' : '' : '' ) : '' !!} value="Chakwal">Chakwal</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Chiniot') ? 'selected' : '' : '' ) : '' !!} value="Chiniot">Chiniot</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Dera Ghazi Khan') ? 'selected' : '' : '' ) : '' !!} value="Dera Ghazi Khan">Dera Ghazi Khan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Faisalabad') ? 'selected' : '' : '' ) : '' !!} value="Faisalabad">Faisalabad</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Gujranwala') ? 'selected' : '' : '' ) : '' !!} value="Gujranwala">Gujranwala</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Gujrat') ? 'selected' : '' : '' ) : '' !!} value="Gujrat">Gujrat</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Hafizabad') ? 'selected' : '' : '' ) : '' !!} value="Hafizabad">Hafizabad</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Jhang') ? 'selected' : '' : '' ) : '' !!} value="Jhang">Jhang</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Jhelum') ? 'selected' : '' : '' ) : '' !!} value="Jhelum">Jhelum</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Kasur') ? 'selected' : '' : '' ) : '' !!} value="Kasur">Kasūr</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Khanewal') ? 'selected' : '' : '' ) : '' !!} value="Khanewal">Khanewal</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Khushab') ? 'selected' : '' : '' ) : '' !!} value="Khushab">Khushab</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Kundian') ? 'selected' : '' : '' ) : '' !!} value="Kundian">Kundian</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Lahore') ? 'selected' : '' : '' ) : '' !!} value="Lahore">Lahore</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Leiah') ? 'selected' : '' : '' ) : '' !!} value="Leiah">Leiah</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Lodhran') ? 'selected' : '' : '' ) : '' !!} value="Lodhran">Lodhran</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Mandi Bahauddin') ? 'selected' : '' : '' ) : '' !!} value="Mandi Bahauddin">Mandi Bahauddin</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Masiwala') ? 'selected' : '' : '' ) : '' !!} value="Masiwala">Masiwala</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Mianwali') ? 'selected' : '' : '' ) : '' !!} value="Mianwali">Mianwali</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Multan') ? 'selected' : '' : '' ) : '' !!} value="Multan">Multan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Muzaffargarh') ? 'selected' : '' : '' ) : '' !!} value="Muzaffargarh">Muzaffargarh</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Nankana Sahib') ? 'selected' : '' : '' ) : '' !!} value="Nankana Sahib">Nankana Sahib</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Narowal') ? 'selected' : '' : '' ) : '' !!} value="Narowal">Narowal</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Okara') ? 'selected' : '' : '' ) : '' !!} value="Okara">Okara</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Pakpattan') ? 'selected' : '' : '' ) : '' !!} value="Pakpattan">Pakpattan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Rahimyar Khan') ? 'selected' : '' : '' ) : '' !!} value="Rahimyar Khan">Rahimyar Khan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Rajanpur') ? 'selected' : '' : '' ) : '' !!} value="Rajanpur">Rajanpur</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Rawalpindi') ? 'selected' : '' : '' ) : '' !!} value="Rawalpindi">Rawalpindi</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Sadiqabad') ? 'selected' : '' : '' ) : '' !!} value="Sadiqabad">Sadiqabad</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Sahiwal') ? 'selected' : '' : '' ) : '' !!} value="Sahiwal">Sahiwal</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Sargodha') ? 'selected' : '' : '' ) : '' !!} value="Sargodha">Sargodha</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Sheikhupura') ? 'selected' : '' : '' ) : '' !!} value="Sheikhupura">Sheikhupura</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Sialkot') ? 'selected' : '' : '' ) : '' !!} value="Sialkot">Sialkot</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Toba Tek Singh') ? 'selected' : '' : '' ) : '' !!} value="Toba Tek Singh">Toba Tek Singh</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Vihari') ? 'selected' : '' : '' ) : '' !!} value="Vihari">Vihari</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Badin') ? 'selected' : '' : '' ) : '' !!} value="Badin">Badin</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Dadu') ? 'selected' : '' : '' ) : '' !!} value="Dadu">Dadu</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Ghotki') ? 'selected' : '' : '' ) : '' !!} value="Ghotki">Ghotki</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Hyderabad') ? 'selected' : '' : '' ) : '' !!} value="Hyderabad">Hyderabad</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Jacobabad') ? 'selected' : '' : '' ) : '' !!} value="Jacobabad">Jacobabad</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Jamshoro') ? 'selected' : '' : '' ) : '' !!} value="Jamshoro">Jamshoro</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Kandhkot') ? 'selected' : '' : '' ) : '' !!} value="Kandhkot">Kandhkot</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Karachi') ? 'selected' : '' : '' ) : '' !!} value="Karachi">Karachi</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Khairpur') ? 'selected' : '' : '' ) : '' !!} value="Khairpur">Khairpur</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Larkana') ? 'selected' : '' : '' ) : '' !!} value="Larkana">Larkana</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Matiari') ? 'selected' : '' : '' ) : '' !!} value="Matiari">Matiari</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Mirpur Khas') ? 'selected' : '' : '' ) : '' !!} value="Mirpur Khas">Mirpur Khas</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Naushahro Firoz') ? 'selected' : '' : '' ) : '' !!} value="Naushahro Firoz">Naushahro Firoz</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Nawabshah') ? 'selected' : '' : '' ) : '' !!} value="Nawabshah">Nawabshah</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Sanghar') ? 'selected' : '' : '' ) : '' !!} value="Sanghar">Sanghar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Shahdad Kot') ? 'selected' : '' : '' ) : '' !!} value="Shahdad Kot">Shahdad Kot</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Shikarpur') ? 'selected' : '' : '' ) : '' !!} value="Shikarpur">Shikarpur</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Sukkur') ? 'selected' : '' : '' ) : '' !!} value="Sukkur">Sukkur</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Tando Allahyar') ? 'selected' : '' : '' ) : '' !!} value="Tando Allahyar">Tando Allahyar</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Tando Muhammad Khan') ? 'selected' : '' : '' ) : '' !!} value="Tando Muhammad Khan">Tando Muhammad Khan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Thatta') ? 'selected' : '' : '' ) : '' !!} value="Thatta">Thatta</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Umarkot') ? 'selected' : '' : '' ) : '' !!} value="Umarkot">Umarkot</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->CITY != "" && $candidateProfile->CITY != NULL) ? ($candidateProfile->CITY == 'Other') ? 'selected' : '' : '' ) : '' !!} value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Province</label>
                                            <select name="province" id="" class="form-control square">
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->PROVINCE != "" && $candidateProfile->PROVINCE != NULL) ? (($candidateProfile->PROVINCE == 'Azad Kashmir') ? 'selected' : '') : '' ) : '' !!} value="Azad Kashmir">Azad Kashmir</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->PROVINCE != "" && $candidateProfile->PROVINCE != NULL) ? (($candidateProfile->PROVINCE == 'Balochistan') ? 'selected' : '') : '' ) : '' !!} value="Balochistan">Balochistan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->PROVINCE != "" && $candidateProfile->PROVINCE != NULL) ? (($candidateProfile->PROVINCE == 'Gilgit-Baltistan') ? 'selected' : '') : '' ) : '' !!} value="Gilgit-Baltistan">Gilgit-Baltistan</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->PROVINCE != "" && $candidateProfile->PROVINCE != NULL) ? (($candidateProfile->PROVINCE == 'Khyber Pakhtunkhwa') ? 'selected' : '') : '' ) : '' !!} value="Khyber Pakhtunkhwa">Khyber Pakhtunkhwa</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->PROVINCE != "" && $candidateProfile->PROVINCE != NULL) ? (($candidateProfile->PROVINCE == 'Punjab') ? 'selected' : '') : '' ) : '' !!} value="Punjab">Punjab</option>
                                                <option {!! ($candidateProfile != NULL) ? (($candidateProfile->PROVINCE != "" && $candidateProfile->PROVINCE != NULL) ? (($candidateProfile->PROVINCE == 'Sindh') ? 'selected' : '') : '' ) : '' !!} value="Sindh">Sindh</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Country</label>
                                            <select name="country" id="" class="form-control square">
                                                <option value="Pakistan" selected>Pakistan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Residence Landline No.</label>
                                            <input type="text" name="residenceNo" class="form-control square" placeholder="Residence Landline Number" maxlength="20" value="{!! ($candidateProfile != NULL) ? $candidateProfile->LANDLINE_NUM : '' !!}">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="" style="font-size: 0.9em;">Mobile No.</label>
                                                <input type="text" name="mobileNo" class="form-control square" placeholder="+923001234567" maxlength="13" required value="{!! ($candidateProfile != NULL) ? $candidateProfile->MOBILE_NUM : '' !!}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Email</label>
                                            <input type="email" name="email" class="form-control square" placeholder="Email" maxlength="50" required value="{!! ($candidateProfile != NULL) ? $candidateProfile->EMAIL : '' !!}">
                                        </div>
                                    </div>
                                </div>
                                @include("admin.qualification" , ["qualification" => $candidateProfile->qualification])
                                @include("admin.experience", ["experience" => $candidateProfile->experience])
                                @include("admin.other-details", ["experience" => $candidateProfile->experience])
                                <div class="row">
                                    @include("admin.skills", ["skill" => $candidateProfile->skill])
                                    @include("admin.interests", ["categories" => $categories,"interest" =>$candidateProfile->interest])
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-5"></div>
                                    <div class="col-sm-3">
                                        <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton" style="font-size: 1.2em;width: 15em;">Update Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
    $(document).ready(function() {
        $("#flUpload").change(function ()
        {
            $("#lblSize").html("");
            var iSize = ($("#flUpload")[0].files[0].size / 1024);
            if (iSize / 1024 > 1)
            {
                $("#lblSize").html( "File size exceeding the Limit");
                $("#flUpload").html('');
                $("#flUpload").val('');
            }
            else
            {
                iSize = (Math.round(iSize * 100) / 100);
                if(iSize > 200)
                {
                    $("#lblSize").html( "File size exceeding the Limit");
                    $("#flUpload").html('');
                    $("#flUpload").val('');
                }
            }
        });
        $("#cvUpload").change(function ()
        {
            error = false;
            $("#cvLblSize").html("");
            var iSize = ($("#cvUpload")[0].files[0].size / 1024);
            if (iSize / 1024 > 1)
            {
                if (((iSize / 1024) / 1024) > 1)
                {
                    error = true;
                    $("#cvLblSize").html( "File size exceeding the Limit");
                    $("#cvUpload").html('');
                    $("#cvUpload").val('');
                }
                else
                {
                    iSize = (Math.round((iSize / 1024) * 100) / 100);
                    if(iSize > 2)
                    {
                        error = true;
                        $("#cvLblSize").html( "File size exceeding the Limit");
                        $("#cvUpload").html('');
                        $("#cvUpload").val('');
                    }
                }
            }
            if(! error)
            {
                $("#cvLblSize").html( "File uploaded !");
            }
        });
    });



    /*function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#accountPicture')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }*/
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });

</script>

    {{--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript">
        function showPreview(objFileInput) {
            if (objFileInput.files[0]) {
                var fileReader = new FileReader();
                fileReader.onload = function (e) {
                    $("#targetLayer").html('<img src="'+e.target.result+'" width="150px" height="150px" class="upload-preview" />');
                    $(".icon-choose-image").css('opacity','0.5');
                }
                fileReader.readAsDataURL(objFileInput.files[0]);
            }
        }
    </script>--}}

    <script>
        $(function() {
            $(document).on('change', ':file', function() {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
            $(document).ready( function() {
                $(':file').on('fileselect', function(event, numFiles, label) {
                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;
                    if( input.length ) {
                        input.val(log);
                    }
                });
            });
            $(document).ready(function(){
               setTimeout(function(){
                   $(".alertContainer").css("display"  , "none");
               } , 3000);
            });
        });
    </script>



@endsection