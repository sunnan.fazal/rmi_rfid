@extends('admin.base')
@section("content")

    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Advertisement
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <form action="{!! url("/save-advertisements") !!}" method="post" enctype="multipart/form-data">
                        <div class="header">
                            <h2><strong>Basic</strong> Information</h2>

                        </div>
                            <div class="body">

                                    {!! csrf_field() !!}

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Enter Job Title <span style="color:red">*</span></label>
                                                <input type="text" class="form-control square" name="jobTitle" placeholder="Job Title" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Select Category <span style="color:red">*</span></label>
                                                <select name="category" id="" class="form-control square" required>
                                                    @foreach ($category as $cat)
                                                        <option value="{!! $cat->ID !!}">{!! $cat->CATEGORY_NAME !!}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Department <span style="color:red">*</span></label>
                                                <select name="department" class="form-control square show-tick" required>
                                                    @foreach($department as $dept)
                                                        <option value="{!! $dept->ID !!}">{!! $dept->DEPARTMENT_NAME !!}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Specialization</label>
                                                <input type="text" class="form-control square" name="specialization" placeholder="Specialization" >
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                <div class="row clearfix">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Job Unit </label>
                                            <select name="jobUnit" class="form-control square show-tick" required>
                                                <option value="RMI">RMI - Rehman Medical Institute</option>
                                                <option value="RMC">RMC - Rehman Medical College</option>
                                                <option value="RCD">RCD</option>
                                                <option value="RCN">RCN</option>
                                                <option value="RCAHS">RCAHS - Rehman College of Allied Health Sciences</option>
                                                <option value="RCRS">RCRS</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Select Job Type <span style="color:red">*</span></label>
                                            <select name="jobType" class="form-control square show-tick" required>
                                                @foreach ($jobtype as $job)
                                                    <option value="{!! $job->ID !!}">{!! $job->JOB_TYPE_NAME !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Select Location <span style="color:red">*</span></label>
                                            <select name="city" id="" class="form-control square" required>
                                                @foreach ($city as $cit)
                                                    <option value="{!! $cit->ID !!}">{!! $cit->CITY_NAME !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Gender Preference</label>
                                            <select name="gender" class="form-control square show-tick">
                                                <option value="No Preference">No Preference</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <br>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="">Enter Job Description<span style="color:red">*</span></label>
                                                <textarea class="form-control" name="jobDescription" placeholder="Enter Job Description" rows="10" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row clearfix">

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Qualification Degree / Certificate <span style="color:red">*</span></label>
                                                <input type="text" class="form-control square" name="minimumEducation" placeholder="Qualification Degree / Certificate" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Qualification Level</label>
                                                <select class="form-control square" name="requiredDegree">
                                                    <option value="Marticulation">Marticulation</option>
                                                    <option value="Faculty of Science/Arts">Faculty of Science/Arts</option>
                                                    <option value="Bachelors">Bachelors</option>
                                                    <option value="Bachelors (2 Years)">Bachelors (2 Years)</option>
                                                    <option value="Bachelors (4 Years)">Bachelors (4 Years)</option>
                                                    <option value="Masters">Masters</option>
                                                    <option value="MS / M.Phil">MS / M.Phil</option>
                                                    <option value="PhD">PhD</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Total Positions</label>
                                                <input type="text" class="form-control square" name="totalPositions" placeholder="Total Positions" >
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Relevant Exp. </label>
                                                <input type="text" class="form-control square" name="relevantExp" placeholder="Relevant Experience" required>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row clearfix">

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Shift <span style="color:red">*</span></label>
                                                <select class="form-control square" name="shifts">
                                                    <option value="All Shifts">All Shifts</option>
                                                    <option value="General">General</option>
                                                    <option value="Morning">Morning</option>
                                                    <option value="Evening">Evening</option>
                                                    <option value="Night">Night</option>
                                                    <option value="Night">Roaster</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Salary </label>
                                                <select class="form-control square" name="salary">
                                                    <option value="Attractive Salary">Attractive Salary</option>
                                                    <option value="> 16,000"> > 16,000</option>
                                                    <option value="16,000 - 25,000">16,000 - 25,000</option>
                                                    <option value="25,000 - 35,000">25,000 - 35,000</option>
                                                    <option value="35,000 - 50,000">35,000 - 50,000</option>
                                                    <option value="50,000 - 75,000">50,000 - 75,000</option>
                                                    <option value="75,000 - 100,000">75,000 - 100,000</option>
                                                    <option value="100,000+">100,000+</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Posted On <span style="color:red">*</span></label>
                                                <input type="date" class="form-control square postedDate" name="postedOn" placeholder="Posted On" required>
                                                <p style="color:red;display: none;font-size: 11px" class="postDateAlert">Cannot Submit Post in Back Date !</p>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Apply Before <span style="color:red">*</span></label>
                                                <input type="date" class="form-control square applyDate" name="applyBefore" placeholder="Apply Before" required>
                                                <p style="color:red;display: none;font-size: 11px" class="applyDateAlert">Last date must be greater then post date !</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <hr>
                                    <br>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton">Submit Post</button>
                                        </div>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{!! url("/resources/assets/ckeditor/ckeditor.js") !!}"></script>
    <script>
        CKEDITOR.replace('jobDescription');
    </script>
    <script>
        $(document).ready(function(){
           $(".postedDate").on("change",function(){

               var today = new Date();
               var dd = today.getDate();

               var mm = today.getMonth()+1;
               var yyyy = today.getFullYear();
               if(dd<10)
               {
                   dd='0'+dd;
               }
               if(mm<10)
               {
                   mm='0'+mm;
               }
               today = yyyy+"-"+mm+'-'+dd;

               if($(this).val() <  today)
               {
                   $(".postDateAlert").css("display","block");
                   $(".rmiDarkButton").css("display","none");
               }
               else {
                   $(".postDateAlert").css("display","none");
                   $(".rmiDarkButton").css("display","block");
               }
           });
           $(".applyDate").on("change",function(){
               var today = new Date();
               var dd = today.getDate();

               var mm = today.getMonth()+1;
               var yyyy = today.getFullYear();
               if(dd<10)
               {
                   dd='0'+dd;
               }
               if(mm<10)
               {
                   mm='0'+mm;
               }
               today = yyyy+"-"+mm+'-'+dd;
               if($(this).val() <  today)
               {
                   $(".applyDateAlert").css("display","block");
                   $(".rmiDarkButton").css("display","none");
               }
               else {
                   $(".applyDateAlert").css("display","none");
                   $(".rmiDarkButton").css("display","block");
               }
           });
        });
    </script>
@endsection