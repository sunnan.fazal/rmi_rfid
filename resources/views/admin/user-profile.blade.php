@extends('admin.base')
@section("content")

    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>{!! $candidateProfile->FIRST_NAME !!} {!! $candidateProfile->LAST_NAME !!} Profile</h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header" style="background-color: #f4f4f4;">
                            <h2><strong>Basic</strong> Information</h2>
                        </div>
                        <hr>
                        <div class="body">
                                <div class="row clearfix">
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">First Name</label>
                                                    <input type="text" class="form-control square" name="firstName" placeholder="First Name" required maxlength="50" value="{!! ($candidateProfile != NULL) ? $candidateProfile->FIRST_NAME : '' !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Last Name</label>
                                                    <input type="text" class="form-control square" name="lastName" placeholder="Last Name" required maxlength="100" value="{!! ($candidateProfile != NULL) ? $candidateProfile->LAST_NAME : '' !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Father / Spouse Name</label>
                                                    <input type="text" class="form-control square" name="fatherSpouseName" placeholder="Father/Spouse Name" required maxlength="100" value="{!! ($candidateProfile != NULL) ? $candidateProfile->FATHER_SPOUSE : '' !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Gender</label>
                                                    <input type="text" class="form-control square" maxlength="50" value="{!! ($candidateProfile != NULL) ? $candidateProfile->GENDER : '' !!}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Date of Birth</label>
                                                    <input type="date" class="form-control square" name="dob" required maxlength="20" value="{!! ($candidateProfile != NULL) ? $candidateProfile->DOB : '' !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Martial Status</label>
                                                    <input type="text" class="form-control square" maxlength="50" value="{!! ($candidateProfile != NULL) ? $candidateProfile->MARITAL_STATUS : '' !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Religion</label>
                                                    <input type="text" class="form-control square" placeholder="Religion" maxlength="50" value="{!! ($candidateProfile != NULL) ? $candidateProfile->RELIGION : '' !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Blood Group</label>
                                                    <input type="text" class="form-control square"  maxlength="50" value="{!! ($candidateProfile != NULL) ? $candidateProfile->BLOOD_GROUP : '' !!}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">CNIC / Passport No.</label>
                                                    <input type="text" name="cnic" class="form-control square" placeholder="CNIC / Passport" maxlength="20" value="{!! ($candidateProfile != NULL) ? $candidateProfile->CNIC : '' !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Nationality</label>
                                                    <input type="text" class="form-control square" name="nationality" placeholder="Nationality" required maxlength="50" value="{!! ($candidateProfile != NULL) ? $candidateProfile->NATIONALITY : '' !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Driving License Type</label>
                                                    <input type="text" class="form-control square" name="licenseType" placeholder="Driving License" value="{!! ($candidateProfile != NULL) ? $candidateProfile->DRIVING_LICENSE : '' !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Driving License Number</label>
                                                    <input type="text" class="form-control square" name="licenseNumber" placeholder="Driving License Number" value="{!! ($candidateProfile != NULL) ? $candidateProfile->DRIVING_LICENSE_NUMBER : '' !!}" readonly>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-4" style="text-align: center; ">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <div class="avatar-upload" style="margin: 0px auto ">
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url({!! ($candidateProfile->CANDIDATE_PIC != NULL || $candidateProfile->CANDIDATE_PIC != "") ? url("/storage/app/pics/".$candidateProfile->CANDIDATE_PIC) : url("/resources/assets/icon-person.png") !!});">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="header" style="background-color: #f4f4f4;">
                                    <h2><strong>Contact</strong> Details</h2>
                                </div>
                                <hr>

                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Address</label>
                                            <input type="text" class="form-control square" maxlength="200" name="currentAddress" placeholder="Postal Address" required value="{!! ($candidateProfile != NULL) ? $candidateProfile->ADDRESS : '' !!}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">City</label>
                                            <input type="text" class="form-control square"  required value="{!! ($candidateProfile != NULL) ? $candidateProfile->CITY : '' !!}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Province</label>
                                            <input type="text" class="form-control square" required value="{!! ($candidateProfile != NULL) ? $candidateProfile->PROVINCE : '' !!}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Country</label>
                                            <select name="country" id="" class="form-control square" readonly>
                                                <option value="Pakistan" selected>Pakistan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Residence Landline No.</label>
                                            <input type="text" name="residenceNo" class="form-control square" placeholder="Residence Landline Number" maxlength="20" value="{!! ($candidateProfile != NULL) ? $candidateProfile->LANDLINE_NUM : '' !!}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="" style="font-size: 0.9em;">Mobile No.</label>
                                                <input type="text" name="mobileNo" class="form-control square" placeholder="Mobile No." maxlength="13" required value="{!! ($candidateProfile != NULL) ? $candidateProfile->MOBILE_NUM : '' !!}" readonly>
                                                <p style="color: red;font-size: 10px;">Mobile Number should be +923001234567</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Email</label>
                                            <input type="email" name="email" class="form-control square" placeholder="Email" maxlength="50" required value="{!! ($candidateProfile != NULL) ? $candidateProfile->EMAIL : '' !!}" readonly>
                                        </div>
                                    </div>
                                </div>
                            <div class="header" style="background-color: #f4f4f4;">
                                <h2><strong>Qualification</strong></h2>
                            </div>
                            <hr>
                            <div class="qualifications">
                                @if(isset($candidateProfile->qualification))
                                    @foreach($candidateProfile->qualification as $qualify)
                                        <div class="row clearfix qualificationROW">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Degree</label>
                                                    <input type="text" name="degree[]" id="" class="form-control square" value="{!! $qualify->DEGREE !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Institute</label>
                                                    <input type="text" class="form-control square" name="institution[]" placeholder="Institute Name" value="{!! $qualify->INSTITUTE_NAME !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Passing Year</label>
                                                    <input type="text" class="form-control square" name="institution[]" placeholder="Institute Name" value="{!! $qualify->PASSING_YEAR !!}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label for="" style="font-size: 0.9em;">Grade</label>
                                                    <input type="text" class="form-control square" name="grade[]" placeholder="Grade" value="{!! $qualify->GRADE !!}" readonly>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="header" style="background-color: #f4f4f4;">
                                <h2><strong>Experience</strong></h2>
                            </div>
                            <hr>
                            <div class="experiences">
                                @if(isset($candidateProfile->experience))
                                    @foreach($candidateProfile->experience as $exp)
                                        <div class="experienceROW">
                                            <div class="row clearfix experienceROW">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="" style="font-size: 0.9em;">Organization</label>
                                                        <input type="text" name="organization[]" class="form-control square" placeholder="Organization" maxlength="255" value="{!! $exp->ORGANIZATION !!}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="" style="font-size: 0.9em;">Designation / Position</label>
                                                        <input type="text" name="designation[]" class="form-control square" placeholder="Designation / Position" maxlength="200" value="{!! $exp->DESIGNATION !!}" readonly >
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="" style="font-size: 0.9em;">Speciality</label>
                                                        <input type="text" name="speciality[]" class="form-control square" placeholder="Speciality" maxlength="200" value="{!! $exp->SPECIALITY !!}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label for="" style="font-size: 0.9em;">Start Date</label>
                                                        <input type="date" name="startDate[]" class="form-control square" placeholder="" value="{!! $exp->START_DATE !!}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label for="" style="font-size: 0.9em;">End Date</label>
                                                        <input type="date" name="endDate[]" class="form-control square" placeholder="" value="{!! $exp->END_DATE !!}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label for="" style="font-size: 0.9em;">Last Salary</label>
                                                        <input type="number" name="lastSalary[]" class="form-control square" placeholder="Last Salary" max="1000000" min="0" value="{!! $exp->LAST_SALARY !!}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="" style="font-size: 0.9em;">Leaving Reason</label>
                                                        <input type="text" name="leavingReason[]" class="form-control square" placeholder="Leaving Reason" maxlength="255" value="{!! $exp->LEAVING_REASON !!}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                        <hr>
                                    @endforeach
                                @endif
                            </div>

                            <div class="header" style="background-color: #f4f4f4;">
                                <h2><strong>Other Details</strong></h2>
                            </div>
                            <hr>
                            <div>
                                <div class="row clearfix">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Are you currently employed ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" class="form-control square" name="currentlyEmployed" {!! $candidateProfile->CURRENTLY_EMPLOYED == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;" readonly>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Are you currently under any service bond ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" class="form-control square" name="serviceBond" {!! $candidateProfile->SERVICE_BOND == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;" readonly>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Can we approach your current employer ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" class="form-control square" name="approachCurrentEmployer" {!! $candidateProfile->APPROACH_CURRENT_EMPLOYER == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;" readonly>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Do you have criminal record ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" class="form-control square criminalRecord" name="criminalRecord" {!! $candidateProfile->CRIMINAL_RECORD == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;" readonly>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Do any of your relatives currently work at RMI ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" class="form-control square relative" name="relative" {!! $candidateProfile->RELATIVE_CURRENTLY_WORKING == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;" readonly>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">Disabilities (if any) ?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" class="form-control square disabilities" name="disabilities" {!! $candidateProfile->DISABILITY == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;" readonly>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">If yes, criminal record</label>
                                            <input type="text" name="criminalRecordDetails" class="form-control square criminalRecordDetails" value="{!! ($candidateProfile != NULL) ? $candidateProfile->CRIMINAL_RECORD_DETAIL : '' !!}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">If yes, relative details</label>
                                            <input type="text" name="relativesDetails" class="form-control square relativesDetails" value="{!! ($candidateProfile != NULL) ? $candidateProfile->RELATIVES_DETAIL : '' !!}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="" style="font-size: 0.9em;">If yes, disabilities</label>
                                            <input type="text" name="disabilitiesDetails" class="form-control square disabilitiesDetails" value="{!! ($candidateProfile != NULL) ? $candidateProfile->DISABILITIES_DETAIL : '' !!}" readonly>
                                        </div>
                                    </div>

                                </div>
                                <br>
                            </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="header" style="background-color: #f4f4f4;">
                                            <h2><strong>Skills</strong></h2>
                                        </div>
                                        <hr>
                                        <div class="skills">
                                            @if($candidateProfile->skill)
                                                @foreach($candidateProfile->skill as $skills)
                                                    <div class="row clearfix skillROW">
                                                        <div class="col-sm-5">
                                                            <div class="form-group">
                                                                <label for="" style="font-size: 0.9em;">Skill</label>
                                                                <input type="text" class="form-control square" name="skill[]" value="{!! $skills->SKILL_NAME !!}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <div class="form-group">
                                                                <label for="" style="font-size: 0.9em;">Experience</label>
                                                                <input type="text" class="form-control square" name="skill[]" value="{!! $skills->EXPERIENCE !!} Year(s) of Experience" readonly>
                                                            </div>
                                                        </div>
                                                        <br>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="header" style="background-color: #f4f4f4;">
                                            <h2><strong>Area of Interest</strong></h2>
                                        </div>
                                        <hr>
                                        <div class="Interest">
                                            @if($candidateProfile->interest)
                                                @foreach($candidateProfile->interest as $data)
                                                    <div class="row clearfix InterestROW">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label for="" style="font-size: 0.9em;">Interest Areas</label>
                                                                <input type="text" class="form-control square" value="{!! $data->CATEGORY_NAME !!}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection