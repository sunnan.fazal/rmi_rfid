@extends('admin.base')
@section("content")

    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Advertisement
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        @if (isset($advertisement))
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="header">
                                <h2><strong>Basic</strong> Information</h2>
                            </div>
                            <div class="body">
                                <form action="{!! url("edit-save-advertisements") !!}" method="post" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                    <input type="hidden"  name="advertisementID" value="{!! $advertisement->ID !!}">

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Enter Job Title<span style="color:red">*</span></label>
                                                <input type="text" class="form-control square" name="jobTitle" placeholder="Job Title" value="{!! $advertisement->JOB_TITLE !!}" required >
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Select Category <span style="color:red">*</span></label>
                                                <select name="category" id="" class="form-control square" required>
                                                    @foreach ($category as $cat)
                                                        <option value="{!! $cat->ID !!}" {!! ($cat->ID == $advertisement->CATEGORY_ID) ? 'selected' : '' !!}>{!! $cat->CATEGORY_NAME !!}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Department</label>
                                                <select name="department" class="form-control square show-tick" required>
                                                    @foreach($department as $dept)
                                                        <option value="{!! $dept->ID !!}" {!! $dept->ID == $advertisement->DEPT_ID ? 'selected': '' !!}>{!! $dept->DEPARTMENT_NAME !!}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Specialization</label>
                                                <input type="text" class="form-control square" name="specialization" placeholder="Specialization" value="{!! $advertisement->SPECIALIZATION !!}" >
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Job Unit </label>
                                                <select name="jobUnit" class="form-control square show-tick" required>
                                                    <option {!! $advertisement->JOB_UNIT == "RMI" ? 'selected' : '' !!} value="RMI">RMI</option>
                                                    <option {!! $advertisement->JOB_UNIT == "RMC" ? 'selected' : '' !!} value="RMC">RMC</option>
                                                    <option {!! $advertisement->JOB_UNIT == "RCD" ? 'selected' : '' !!} value="RCD">RCD</option>
                                                    <option {!! $advertisement->JOB_UNIT == "RCN" ? 'selected' : '' !!} value="RCN">RCN</option>
                                                    <option {!! $advertisement->JOB_UNIT == "RCAHS" ? 'selected' : '' !!} value="RCAHS">RCAHS</option>
                                                    <option {!! $advertisement->JOB_UNIT == "RCRS" ? 'selected' : '' !!} value="RCRS">RCRS</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Select Job Type <span style="color:red">*</span></label>
                                                <select name="jobType" class="form-control square show-tick" required>
                                                    @foreach ($jobtype as $job)
                                                        <option value="{!! $job->ID !!}" {!! ($job->ID == $advertisement->JOB_TYPE_ID) ? 'selected' : '' !!}>{!! $job->JOB_TYPE_NAME !!}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Select Location <span style="color:red">*</span></label>
                                                <select name="city" id="" class="form-control square" required>
                                                    @foreach ($city as $cit)
                                                        <option value="{!! $cit->ID !!}" {!! ($cit->ID == $advertisement->CITY_ID) ? 'selected' : '' !!}>{!! $cit->CITY_NAME !!}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Gender </label>
                                                <select name="gender" class="form-control square show-tick" required>
                                                    <option {!! $advertisement->GENDER == "No Preference" ? 'selected' : '' !!} value="No Preference">No Preference</option>
                                                    <option {!! $advertisement->GENDER == "Male" ? 'selected' : '' !!} value="Male">Male</option>
                                                    <option {!! $advertisement->GENDER == "Female" ? 'selected' : '' !!} value="Female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="">Enter Job Description<span style="color:red">*</span></label>
                                                <textarea class="form-control" name="jobDescription" placeholder="Enter Job Description" rows="10" required>{!! $advertisement->JOB_DESCRIPTION !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Qualification Degree / Certificate <span style="color:red">*</span></label>
                                                <input type="text" class="form-control square" name="minimumEducation" value="{!! $advertisement->MINIMUM_EDUCATION !!}" placeholder="Minimum Education" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Qualification Level</label>
                                                <select class="form-control square" name="requiredDegree" >
                                                    <option value="Marticulation" {!! $advertisement->REQUIRED_DEGREE == "Marticulation" ? "selected" : "" !!}>Marticulation</option>
                                                    <option value="Faculty of Science/Arts" {!! $advertisement->REQUIRED_DEGREE == "Faculty of Science/Arts" ? "selected" : "" !!}>Faculty of Science/Arts</option>
                                                    <option value="Bachelors" {!! $advertisement->REQUIRED_DEGREE == "Bachelors" ? "selected" : "" !!}>Bachelors</option>
                                                    <option value="Bachelors (2 Years)" {!! $advertisement->REQUIRED_DEGREE == "Bachelors (2 Years)" ? "selected" : "" !!}>Bachelors (2 Years)</option>
                                                    <option value="Bachelors (4 Years)" {!! $advertisement->REQUIRED_DEGREE == "Bachelors (4 Years)" ? "selected" : "" !!}>Bachelors (4 Years)</option>
                                                    <option value="Masters" {!! $advertisement->REQUIRED_DEGREE == "Masters" ? "selected" : "" !!}>Masters</option>
                                                    <option value="MS / M.Phil" {!! $advertisement->REQUIRED_DEGREE == "MS / M.Phil" ? "selected" : "" !!}>MS / M.Phil</option>
                                                    <option value="PhD" {!! $advertisement->REQUIRED_DEGREE == "PhD" ? "selected" : "" !!}>PhD</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Total Positions</label>
                                                <input type="text" class="form-control square" name="totalPositions" value="{!! $advertisement->TOTAL_POSITIONS !!}" placeholder="Total Positions" >
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Relevant Exp. </label>
                                                <input type="text" class="form-control square" name="relevantExp" value="{!! $advertisement->RELEVANT_EXP !!}" placeholder="Relevant Experience" required>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Shift <span style="color:red">*</span></label>
                                                <select class="form-control square" name="shifts">
                                                    <option value="All Shifts" {!! $advertisement->SHIFT == "All Shifts" ? "selected" : "" !!} >All Shifts</option>
                                                    <option value="General" {!! $advertisement->SHIFT == "General" ? "selected" : "" !!} >General</option>
                                                    <option value="Morning" {!! $advertisement->SHIFT == "Morning" ? "selected" : "" !!} >Morning</option>
                                                    <option value="Evening" {!! $advertisement->SHIFT == "Evening" ? "selected" : "" !!} >Evening</option>
                                                    <option value="Night" {!! $advertisement->SHIFT == "Night" ? "selected" : "" !!}>Night</option>
                                                    <option value="Roaster" {!! $advertisement->SHIFT == "Roaster" ? "selected" : "" !!}>Roaster</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Salary </label>
                                                <select class="form-control square" name="salary">
                                                    <option value="Attractive Salary" {!! $advertisement->SALARY == "Attractive Salary" ? "selected" : "" !!}>Attractive Salary</option>
                                                    <option value="> 16,000" {!! $advertisement->SALARY == "> 16,000" ? "selected" : "" !!}> > 16,000</option>
                                                    <option value="16,000 - 25,000" {!! $advertisement->SALARY == "16,000 - 25,000" ? "selected" : "" !!} >16,000 - 25,000</option>
                                                    <option value="25,000 - 35,000" {!! $advertisement->SALARY == "25,000 - 35,000" ? "selected" : "" !!} >25,000 - 35,000</option>
                                                    <option value="35,000 - 50,000" {!! $advertisement->SALARY == "35,000 - 50,000" ? "selected" : "" !!} >35,000 - 50,000</option>
                                                    <option value="50,000 - 75,000" {!! $advertisement->SALARY == "50,000 - 75,000" ? "selected" : "" !!} >50,000 - 75,000</option>
                                                    <option value="75,000 - 100,000" {!! $advertisement->SALARY == "75,000 - 100,000" ? "selected" : "" !!}>75,000 - 100,000</option>
                                                    <option value="100,000+" {!! $advertisement->SALARY == "100,000+" ? "selected" : "" !!}>100,000+</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Posted On <span style="color:red">*</span></label>
                                                <input type="date" class="form-control square" name="postedOn" placeholder="Posted On" value="{!! $advertisement->POSTED_AT !!}" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="">Apply Before <span style="color:red">*</span></label>
                                                <input type="date" class="form-control square" name="applyBefore" value="{!! $advertisement->APPLY_BEFORE !!}" placeholder="Apply Before" required>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <hr>
                                    <br>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton">Update Advertisement</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </section>
    <script src="{!! url("/resources/assets/ckeditor/ckeditor.js") !!}"></script>
    <script>
        CKEDITOR.replace('jobDescription');
    </script>
    <script>
        $(document).ready(function(){
            $(".postedDate").on("change",function(){

                var today = new Date();
                var dd = today.getDate();

                var mm = today.getMonth()+1;
                var yyyy = today.getFullYear();
                if(dd<10)
                {
                    dd='0'+dd;
                }
                if(mm<10)
                {
                    mm='0'+mm;
                }
                today = yyyy+"-"+mm+'-'+dd;

                if($(this).val() <  today)
                {
                    $(".postDateAlert").css("display","block");
                    $(".rmiDarkButton").css("display","none");
                }
                else {
                    $(".postDateAlert").css("display","none");
                    $(".rmiDarkButton").css("display","block");
                }
            });
            $(".applyDate").on("change",function(){
                var today = new Date();
                var dd = today.getDate();

                var mm = today.getMonth()+1;
                var yyyy = today.getFullYear();
                if(dd<10)
                {
                    dd='0'+dd;
                }
                if(mm<10)
                {
                    mm='0'+mm;
                }
                today = yyyy+"-"+mm+'-'+dd;
                if($(this).val() <  today)
                {
                    $(".applyDateAlert").css("display","block");
                    $(".rmiDarkButton").css("display","none");
                }
                else {
                    $(".applyDateAlert").css("display","none");
                    $(".rmiDarkButton").css("display","block");
                }
            });
        });
    </script>
@endsection