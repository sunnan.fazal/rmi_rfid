<div class="header" style="background-color: #f4f4f4;">
    <h2><strong>Other Details</strong></h2>
</div>
<hr>
<div>
    <div class="row clearfix">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="" style="font-size: 0.9em;">Are you currently employed ?</label>
            </div>
        </div>
        <div class="col-sm-1">
            <input type="checkbox" class="form-control square" name="currentlyEmployed" {!! $candidateProfile->CURRENTLY_EMPLOYED == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;">
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label for="" style="font-size: 0.9em;">Are you currently under any service bond ?</label>
            </div>
        </div>
        <div class="col-sm-1">
            <input type="checkbox" class="form-control square" name="serviceBond" {!! $candidateProfile->SERVICE_BOND == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;">
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label for="" style="font-size: 0.9em;">Can we approach your current employer ?</label>
            </div>
        </div>
        <div class="col-sm-1">
            <input type="checkbox" class="form-control square" name="approachCurrentEmployer" {!! $candidateProfile->APPROACH_CURRENT_EMPLOYER == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;">
        </div>
    </div>
    <br>
    <div class="row clearfix">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="" style="font-size: 0.9em;">Do you have criminal record ?</label>
            </div>
        </div>
        <div class="col-sm-1">
            <input type="checkbox" class="form-control square criminalRecord" name="criminalRecord" {!! $candidateProfile->CRIMINAL_RECORD == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;">
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label for="" style="font-size: 0.9em;">Do any of your relatives currently work at RMI ?</label>
            </div>
        </div>
        <div class="col-sm-1">
            <input type="checkbox" class="form-control square relative" name="relative" {!! $candidateProfile->RELATIVE_CURRENTLY_WORKING == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;">
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label for="" style="font-size: 0.9em;">Disabilities (if any) ?</label>
            </div>
        </div>
        <div class="col-sm-1">
            <input type="checkbox" class="form-control square disabilities" name="disabilities" {!! $candidateProfile->DISABILITY == 1 ? "checked" : "" !!} style="width: 2em;margin-top: -0.7em;">
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-4">
            <div class="form-group">
                <label for="" style="font-size: 0.9em;">If yes, criminal record</label>
                <input type="text" name="criminalRecordDetails" class="form-control square criminalRecordDetails" placeholder="Criminal Record" value="{!! ($candidateProfile != NULL) ? $candidateProfile->CRIMINAL_RECORD_DETAIL : '' !!}">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="" style="font-size: 0.9em;">If yes, relative details</label>
                <input type="text" name="relativesDetails" class="form-control square relativesDetails" placeholder="Relatives" value="{!! ($candidateProfile != NULL) ? $candidateProfile->RELATIVES_DETAIL : '' !!}">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="" style="font-size: 0.9em;">If yes, disabilities</label>
                <input type="text" name="disabilitiesDetails" class="form-control square disabilitiesDetails" placeholder="Disabilities" value="{!! ($candidateProfile != NULL) ? $candidateProfile->DISABILITIES_DETAIL : '' !!}">
            </div>
        </div>

    </div>
    <br>
</div>
<script>
    $(document).ready(function(){

        if(!($(".criminalRecord").is(":checked")))
        {
            $(".criminalRecordDetails").prop("disabled", true);
        }
        if(!($(".relative").is(":checked")))
        {
            $(".relativesDetails").prop("disabled", true);
        }
        if(!($(".disabilities").is(":checked")))
        {
            $(".disabilitiesDetails").prop("disabled", true);
        }

        $(".criminalRecord").on("click" , function(){
            if($(this).is(":checked"))
            {
                $(".criminalRecordDetails").val("");
                $(".criminalRecordDetails").prop("disabled", false);
            }
            else {
                $(".criminalRecordDetails").val("");
                $(".criminalRecordDetails").prop("disabled", true);
            }
        });
        $(".relative").on("click" , function(){
            if($(this).is(":checked"))
            {
                $(".relativesDetails").val("");
                $(".relativesDetails").prop("disabled", false);
            }
            else {
                $(".relativesDetails").val("");
                $(".relativesDetails").prop("disabled", true);
            }
        });
        $(".disabilities").on("click" , function(){
            if($(this).is(":checked"))
            {
                $(".disabilitiesDetails").val("");
                $(".disabilitiesDetails").prop("disabled", false);
            }
            else {
                $(".disabilitiesDetails").val("");
                $(".disabilitiesDetails").prop("disabled", true);
            }
        });
    });
</script>



