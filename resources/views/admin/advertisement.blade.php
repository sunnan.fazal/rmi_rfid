@extends('admin.base')
@section("content")
    <style>
        body{
            overflow: hidden;
        }
    </style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Advertisements
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card student-list">
                        <div class="body">
                            <div class="advertisements" style="height: 88vh"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            $(".advertisements").kendoGrid({
                toolbar: ["pdf","excel"],
                excel: {
                    fileName: "Advertisements List.xlsx",
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url : $("body").attr("data-url")+"/show-all-advertisements",
                            dataType : "json"
                        },
                    },
                    schema: {
                        data : "data",
                        total : "total"
                    },

                    batch: true,
                    pageSize: 25,
                    serverPaging: true,
                    serverFiltering: false,
                    serverSorting: false
                },
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            contains: "Contains",
                            doesnotcontain:"Doesn't Contain",
                            gt: "After",
                            lt: "Before"
                        }
                    }
                },
                groupable: true,
                sortable: true,
                resizable: true,
                pageable: {
                    pageSizes: true,
                    pageSizes: [ 25,50,100,500,1000],
                    refresh: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        template: "<a target='_blank' style='font-weight: bold;color:rgb(31,42,91);'  href='"+$("body").attr("data-url")+"/edit-advertisements/#:ID#'>#:JOB_TITLE#</a>",
                        field: "JOB_TITLE",
                        title: "Job Title",
                        width:250
                    },{
                        field: "SALARY",
                        title: "Salary",
                    },{
                        field: "TOTAL_POSITIONS",
                        title: "Positions",
                    },{
                        field: "SHIFT",
                        title: "Shift",
                    },{
                        field: "APPLY_BEFORE",
                        title: "Last Date",
                    },{
                        field: "STATUS",
                        title: "Approval"
                    },{
                        field: "APPLICATION_STATUS",
                        title: "Status"
                    },{
                        field: "CREATED_AT",
                        title: "Created On"
                    },{
                        field: "POSTED_AT",
                        title: "Posted On"
                    },{
                        field: "CREATED_BY",
                        title: "Created By"
                    },{
                        template:"<div style='text-align: center'><a href='"+$("body").attr("data-url")+"/delete-advertisements/#:ID#' title='In Active Application'><i style='color: red;font-size: 1.5em;' class='zmdi zmdi-close-circle-o'></i></a></div>",
                        field: "ID",
                        title: "Action",
                        "filterable": {
                            cell :{
                                enabled: false,
                            }
                        }
                    }
                ]
            });
        });
    </script>
@endsection