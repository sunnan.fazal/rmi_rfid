@extends('admin.base')
@section("content")
    
<style type="text/css">
    .mainPic {
        width: 100%;
        height:  100%;
    }
    .news_img_1{
        width: 100%;
        height:  100%;
    } 
    .news_img_2{
        width: 100%;
        height:  100%;
    }   
    .news_img_3{
        width: 100%;
        height:  100%;
    } 
    .news_img_4{
        width: 100%;
        height:  100%;
    }
</style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Edit Latest News
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <form action="{!! url("/save-edit-news") !!}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="newsid" value="{{ $news->id }}">
                            <div class="header">
                                <h2><strong>Basic</strong> Information</h2>

                            </div>
                            <div class="body">

                                {!! csrf_field() !!}
                                <div class="row clearfix">
                                    <div class="col-sm-9">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="" style="float: right"><b>Active</b></label>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" name="active" style="" {{ $news->active == 1 ? 'checked' : '' }}>
                                    </div>
                                </div>
                                <hr>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <img class="pull-left flip mr-15 thumbnail mainPic"  src="{{ url('/resources/news/' . $news->news_image) }}" alt="">
                                            <input type="file" name="pic_1" class="form-control pic_1">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <textarea class="form-control newsDescription1" name="newsDescription1">{{ $news->news_image_description }}</textarea>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <textarea class="form-control newsDescription2" name="newsDescription2">{{ $news->news_details }}</textarea>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <img class="pull-left flip mr-15 thumbnail news_img_1" src="{{ url('/resources/news/' . $news->news_image_1) }}" alt="" style="height: 100%">
                                            <input type="file" name="news_pic1" class="form-control news_pic1">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <img class="pull-left flip mr-15 thumbnail news_img_2" src="{{ url('/resources/news/' . $news->news_image_2) }}" alt="" style="height: 100%">
                                            <input type="file" name="news_pic2" class="form-control news_pic2">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <img class="pull-left flip mr-15 thumbnail news_img_3" src="{{ url('/resources/news/' . $news->news_image_3) }}" alt="" style="height: 100%">
                                            <input type="file" name="news_pic3" class="form-control news_pic3">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <img class="pull-left flip mr-15 thumbnail news_img_4" src="{{ url('/resources/news/' . $news->news_image_4) }}" alt="" style="height: 100%">
                                            <input type="file" name="news_pic4" class="form-control news_pic4">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <textarea class="form-control newsDescription3" name="newsDescription3">{{ $news->news_last_description }}</textarea>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <input type="radio" name="newstype" value="Featured News" {{ $news->featured_news == 1 ? 'checked' : '' }}> Featured News
                                        &nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="newstype" value="COVID-19 News" {{ $news->covid_news == 1 ? 'checked' : '' }}> COVID-19 News
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton"><b>Save News</b></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{!! url("/resources/assets/ckeditor/ckeditor.js") !!}"></script>
    <script>
        CKEDITOR.replace('newsDescription1');
        CKEDITOR.replace('newsDescription2');
        CKEDITOR.replace('newsDescription3');
        
        $(function () {
            $(".pic_1").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".news_pic1").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_1;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".news_pic2").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_2;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".news_pic3").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_3;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".news_pic4").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_4;
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });

        function imageIsLoaded(e) {
            $('.mainPic').attr('src', e.target.result);
        };
        function imageIsLoaded_1(e) {
            $('.news_img_1').attr('src', e.target.result);
        };
        function imageIsLoaded_2(e) {
            $('.news_img_2').attr('src', e.target.result);
        };
        function imageIsLoaded_3(e) {
            $('.news_img_3').attr('src', e.target.result);
        };
        function imageIsLoaded_4(e) {
            $('.news_img_4').attr('src', e.target.result);
        };
    </script>
@endsection