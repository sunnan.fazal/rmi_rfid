@extends('admin.base')
@section("content")
    <style>
        body{
            overflow: hidden;
        }
    </style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>All Tenders
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="body">
                                <a href="{{ url('/create-tender') }}" class="btn btn-primary" style="float: right">Create Tender</a>
                            <br>
                            <br>
                            <br>
                                <table class="table table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Tender Id</th>
                                        <th>Name</th>
                                        <th>Procurement Category</th>
                                        <th>Advertised at</th>
                                        <th>Closing</th>
                                        <th>Tender Adv.</th>
                                        <th>Corrigendum</th>
                                        <th>SBD</th>
                                        <th>Technical Evaluation</th>
                                        <th>Active</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach( $tender as $tend)
                                        <tr>
                                            <td>{!! $tend->id !!}</td>
                                            <td>{!! $tend->tender_name !!}</td>
                                            <td>{!! $tend->procurement_entity !!}</td>
                                            <td>{!! date('d M, Y' , strtotime($tend->advertisement_date)) !!}</td>
                                            <td>{!! date('d M,Y' , strtotime($tend->closing_date)) !!}</td>
                                            <td><a target="_blank"  href="{!! url('resources/tenders/'.$tend->tender_eoi) !!}">{{ ($tend->tender_eoi != NULL) ? 'Document' : '' }}</a></td>
                                            <td><a target="_blank"  href="{!! url('resources/tenders/'.$tend->tender_eoi_2) !!}">{{ ($tend->tender_eoi_2 != NULL) ? 'Document' : '' }}</a></td>
                                            <td><a target="_blank"  href="{!! url('resources/tenders/'.$tend->sbd) !!}">{{ ($tend->sbd != NULL) ? 'Document' : '' }}</a></td>
                                            <td><a target="_blank"  href="{!! url('resources/tenders/'.$tend->tender_evaluation) !!}">{{ ($tend->tender_evaluation != NULL) ? 'Document' : '' }}</a></td>
                                            <td><input type="checkbox" {!! $tend->active == 1 ? 'checked' : '' !!}></td>
                                            <td><a style="color: darkblue" href="{!! url('/edit-tender/' . $tend->id) !!}"><i class="zmdi zmdi-edit"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            $('.table').DataTable();
        });
    </script>
   
@endsection