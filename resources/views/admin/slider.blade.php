@extends('admin.base')
@section("content")
    <style>
        body{
            overflow: hidden;
        }
    </style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>All Sliders
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="body">
                            <a href="{{ url('/create-slider') }}" class="btn btn-primary" style="float: right">Create Slider</a>
                            <br>
                            <br>
                            <br>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Slider #</th>
                                    <th>Description</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $slider as $slide)
                                    <tr>
                                        <td>{!! $slide->id !!}</td>
                                        <td>{!! $slide->slider_text !!}</td>
                                        <td>
                                            <a style="color: darkblue" href="{!! url('/edit-slider/' . $slide->id) !!}"><i class="zmdi zmdi-edit"></i></a>&nbsp;&nbsp;&nbsp;
                                            <a style="color: darkblue" href="#"><i class="zmdi zmdi-delete"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            $('.table').DataTable();
        });
    </script>

@endsection