<div class="col-sm-6">
    <div class="header" style="background-color: #f4f4f4;">
        <h2><strong>Area of Interest</strong></h2>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-10"></div>
        <div class="col-sm-2">
            <a class="addRowInterest"><i class="material-icons" style="color: lightgreen;font-size: 3em;cursor: pointer;">add_box</i></a>
        </div>
    </div>
    <div class="Interest">
        @if($interest)
            @foreach($interest as $data)
                <div class="row clearfix InterestROW">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="" style="font-size: 0.9em;">Interest Areas</label>
                            <select name="interest[]" id="" class="form-control square">
                                @foreach($categories as $cat)
                                    @foreach($cat as $allCat)
                                        <option {!! $allCat->CATEGORY_NAME == $data->CATEGORY_NAME ? "selected" :"" !!} value="{!! $allCat->CATEGORY_NAME !!}">{!! $allCat->CATEGORY_NAME !!}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2">
                        <a data-id="{!! $data->ID !!}" onclick="removeRowInterest(this)"><i class="material-icons" style="color: lightcoral;font-size: 3em;margin-top: 0.6em;cursor:pointer;">indeterminate_check_box</i></a>
                    </div>
                    <br>
                </div>
            @endforeach
        @endif
    </div>
    <script type="text/html" id="InterestROWS">
        <div class="row clearfix InterestROW">
            <div class="col-sm-8">
                <div class="form-group">
                    <label for="" style="font-size: 0.9em;">Interest Areas</label>
                    <select name="interest[]" id="" class="form-control square">
                        <option value="">-- Select Category --</option>
                        @foreach($categories as $cat)
                            @foreach($cat as $data)
                                <option value="{!! $data->CATEGORY_NAME !!}">{!! $data->CATEGORY_NAME !!}</option>
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-2">
                <a data-id="" onclick="removeRowInterest(this)"><i class="material-icons" style="color: lightcoral;font-size: 3em;margin-top: 0.6em;cursor:pointer;">indeterminate_check_box</i></a>
            </div>
            <br>
        </div>
    </script>
    <script>
        $(document).ready(function () {
            $(".Interest").append($("#InterestROWS").html());
            $(".addRowInterest").on("click" ,function(){
                $(".Interest").append($("#InterestROWS").html());
            });
        });
        function removeRowInterest(val)
        {
            if($(val).attr("data-id") != "")
            {
                $.get($("body").attr("data-url")+"/remove-interest/"+$(val).attr("data-id"), function(data, status){
                    $(val).closest("div.InterestROW").remove();
                });
            }
            $(val).closest("div.InterestROW").remove();
        }
    </script>
</div>
