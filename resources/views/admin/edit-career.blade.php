@extends('admin.base')
@section("content")

    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Edit Advertisement
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <form action="{!! url("/save-edit-advertisement") !!}" method="post" enctype="multipart/form-data">
                            <div class="header">
                                <h2><strong>Basic</strong> Information</h2>

                            </div>
                            <div class="body">
                                <input type="hidden" value="{{ $career->id }}" name="career_id">
                                {!! csrf_field() !!}
                                <div class="row clearfix">
                                    <div class="col-sm-9">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="" style="float: right"><b>Active</b></label>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" name="active" style="" {{ $career->active == 1 ? 'checked' : '' }}>
                                    </div>
                                </div>
                                <hr>
                                <div class="row clearfix">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Advertisement Description <span style="color:red">*</span></label>
                                            <input type="text" class="form-control square" name="career_description" placeholder="Description" required value="{{ $career->description }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Type <span style="color:red">*</span></label>
                                            <select name="career_type" class="form-control">
                                                <option value="Contract" {{ ($career->type == "Contract") ? 'selected' : '' }}>Contract</option>
                                                <option value="Permanent" {{ ($career->type == "Permanent") ? 'selected' : '' }}>Permanent</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Advertisement Date <span style="color:red">*</span></label>
                                            <input type="date" class="form-control square" name="advertisement_date" placeholder="Advertisement Date" required value="{{ $career->advertisement_date }}"> 
                                        </div>
                                    </div><div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Closing Date <span style="color:red">*</span></label>
                                            <input type="date" class="form-control square" name="closing_date" placeholder="Closing Date" value="{{ $career->closing_date }}"> 
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="">Attachment <span style="color:red">*</span></label>
                                            <input type="file" name="file" class="form-control">
                                            <a target="_blank" href="{{ url('resources/careers/'.$career->file) }}" style="color: red">{{ $career->file != NULL ? 'Attachment !' : '' }}</a>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton"><b>Save Advertisement</b></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection