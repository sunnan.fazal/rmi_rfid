@extends('admin.base')
@section("content")
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css">
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Dashboard
                        <small>Welcome to Peshawar Institute of Cardiology</small>
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;"><i class="zmdi zmdi-power"></i></h6></a>
                </div>

            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-6">
                            <div class="card top_counter">
                                <div class="body">
                                    <div class="icon xl-slategray"><i class="zmdi zmdi-assignment"></i> </div>
                                    <div class="content">
                                        <div class="text">Active Tenders</div>
                                        <h5 class="number count-to" data-from="0" data-to="0" data-speed="10000" data-fresh-interval="700">{{ $tenders }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="card top_counter">
                                <div class="body">
                                    <div class="icon xl-slategray"><i class="zmdi zmdi-account"></i> </div>
                                    <div class="content">
                                        <div class="text">Sliders</div>
                                        <h5 class="number count-to" data-from="0" data-to="" data-speed="2500" data-fresh-interval="700">0</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="card top_counter">
                                <div class="body">
                                    <div class="icon xl-slategray"><i class="zmdi zmdi-copy"></i> </div>
                                    <div class="content">
                                        <div class="text">Users</div>
                                        <h5 class="number count-to" data-from="0" data-to="" data-speed="2500" data-fresh-interval="700">0</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
