@extends('admin.base')
@section("content")
    <style>
        body{
            overflow: hidden;
        }
    </style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>All Employees
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card student-list">
                        <div class="body">
                            <div class="employees" style="height: 88vh"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--<script>
        $(document).ready(function(){
            $('.candidates').DataTable();
        });
    </script>--}}
    <script>
        $(document).ready(function () {
            $(".employees").kendoGrid({
                toolbar: ["pdf","excel"],
                excel: {
                    fileName: "Employees List.xlsx",
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url : $("body").attr("data-url")+"/get-all-employees",
                            dataType : "json"
                        },
                    },
                    schema: {
                        data : "data",
                        total : "total",
                    },

                    batch: true,
                    pageSize: 500,
                    serverPaging: true,
                    serverFiltering: false,
                    serverSorting: false
                },
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            contains: "Contains",
                            doesnotcontain:"Doesn't Contain",
                            gt: "After",
                            lt: "Before"
                        }
                    }
                },
                groupable: true,
                sortable: true,
                resizable: true,
                pageable: {
                    pageSizes: true,
                    pageSizes: [ 500,1000,2000,'All'],
                    refresh: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        field: "f_ConsumerID",
                        title: "S#",
                        width : 15,
                        "filterable": {
                            cell :{
                                enabled: false,
                            }
                        }
                    },{
                        field: "f_ConsumerNO",
                        title: "Employee#",
                        width : 28,
                    },{
                        template : "<a href='get-employee-swipes/#=f_ConsumerID#'>#:f_ConsumerName#</a>",
                        field: "f_ConsumerName",
                        title: "Name",
                        width : 100,
                    },{
                        field: "Mobile",
                        title: "Mobile",
                        width : 50,
                    },{
                        field: "Ext",
                        title: "Ext",
                        width : 50,
                    },{
                        field: "groupName",
                        title: "Department",
                        width : 100,
                    },{
                        field: "f_BeginYMD",
                        title: "Registered On",
                        width: 50,
                    }
                ]
            });
        });
    </script>
@endsection