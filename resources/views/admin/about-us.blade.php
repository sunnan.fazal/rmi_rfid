@extends('admin.base')
@section("content")
    
<style type="text/css">
    .content_pic_1 {
        width: 100%;
        height:  400px;
    }
    .content_pic_2{
        width: 100%;
        height:  250px;
    } 
    .content_pic_3 {
        width: 100%;
        height:  250px;
    }
    .content_pic_4 {
        width: 100%;
        height:  250px;
    }.content_pic_5 {
        width: 100%;
        height:  250px;
    }.content_pic_6 {
        width: 100%;
        height:  250px;
    }.content_pic_7 {
        width: 100%;
        height:  250px;
    }.content_pic_8 {
        width: 100%;
        height:  250px;
    }.content_pic_9 {
        width: 100%;
        height:  250px;
    }.content_pic_10 {
        width: 100%;
        height:  250px;
    }   
</style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Create About Us
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <form action="{!! url("/save-about-us") !!}" method="post" enctype="multipart/form-data">
                            <div class="header">
                                <h2><strong>Basic</strong> Information</h2>

                            </div>
                            <div class="body">
                                {!! csrf_field() !!}
                                
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            @if(count($aboutus) > 0)
                                                <img class="pull-left flip mr-15 thumbnail content_pic_1"  src="{{ url('/resources/aboutus/' . $aboutus->first()->banner_image  )}}" alt="">
                                            @else
                                                <img class="pull-left flip mr-15 thumbnail content_pic_1"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="banner_image" class="form-control pic_1">
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">                                     
                                            <textarea class="form-control ckeditor" name="details">{{ (count($aboutus) > 0 ) ? $aboutus->first()->details : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($aboutus) > 0)
                                                <img class="pull-left flip mr-15 thumbnail content_pic_2"  src="{{ url('/resources/aboutus/' . $aboutus->first()->image_1  )}}" alt="">
                                            @else
                                                <img class="pull-left flip mr-15 thumbnail content_pic_2"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="image_1" class="form-control pic_2" >
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($aboutus) > 0)
                                                <img class="pull-left flip mr-15 thumbnail content_pic_3"  src="{{ url('/resources/aboutus/' . $aboutus->first()->image_2  )}}" alt="">
                                            @else
                                                <img class="pull-left flip mr-15 thumbnail content_pic_3"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="image_2" class="form-control pic_3" >
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($aboutus) > 0)
                                                <img class="pull-left flip mr-15 thumbnail content_pic_4"  src="{{ url('/resources/aboutus/' . $aboutus->first()->image_3  )}}" alt="">
                                            @else
                                                <img class="pull-left flip mr-15 thumbnail content_pic_4"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="image_3" class="form-control pic_4" >
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">                                     
                                            <textarea class="form-control ckeditor" name="details_2">{{ (count($aboutus) > 0 ) ? $aboutus->first()->details_2 : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($aboutus) > 0)
                                                <img class="pull-left flip mr-15 thumbnail content_pic_5"  src="{{ url('/resources/aboutus/' . $aboutus->first()->image_4  )}}" alt="">
                                            @else
                                                <img class="pull-left flip mr-15 thumbnail content_pic_5"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="image_4" class="form-control pic_5" >
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($aboutus) > 0)
                                                <img class="pull-left flip mr-15 thumbnail content_pic_6"  src="{{ url('/resources/aboutus/' . $aboutus->first()->image_5  )}}" alt="">
                                            @else
                                                <img class="pull-left flip mr-15 thumbnail content_pic_6"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="image_5" class="form-control pic_6" >
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($aboutus) > 0)
                                                <img class="pull-left flip mr-15 thumbnail content_pic_7"  src="{{ url('/resources/aboutus/' . $aboutus->first()->image_6  )}}" alt="">
                                            @else
                                                <img class="pull-left flip mr-15 thumbnail content_pic_7"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="image_6" class="form-control pic_7" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">                                     
                                            <textarea class="form-control ckeditor" name="details_3">{{ (count($aboutus) > 0 ) ? $aboutus->first()->details_3 : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($aboutus) > 0)
                                                <img class="pull-left flip mr-15 thumbnail content_pic_8"  src="{{ url('/resources/aboutus/' . $aboutus->first()->image_7  )}}" alt="">
                                            @else
                                                <img class="pull-left flip mr-15 thumbnail content_pic_8"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="image_7" class="form-control pic_8" >
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($aboutus) > 0)
                                                <img class="pull-left flip mr-15 thumbnail content_pic_9"  src="{{ url('/resources/aboutus/' . $aboutus->first()->image_8  )}}" alt="">
                                            @else
                                                <img class="pull-left flip mr-15 thumbnail content_pic_9"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="image_8" class="form-control pic_9" >
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            @if(count($aboutus) > 0)
                                                <img class="pull-left flip mr-15 thumbnail content_pic_10"  src="{{ url('/resources/aboutus/' . $aboutus->first()->image_9  )}}" alt="">
                                            @else
                                                <img class="pull-left flip mr-15 thumbnail content_pic_10"  src="http://placehold.it/430x240" alt="">
                                            @endif
                                            <input type="file" name="image_9" class="form-control pic_10" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton"><b>Save About Us</b></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
           
        </div>
    </section>
    <script>
         $(function () {
            $(".pic_1").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_2").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_1;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_3").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_2;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_4").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_3;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_5").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_4;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_6").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_5;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_7").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_6;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_8").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_7;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_9").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_8;
                    reader.readAsDataURL(this.files[0]);
                }
            });
            $(".pic_10").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_9;
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });

        function imageIsLoaded(e) {
            $('.content_pic_1').attr('src', e.target.result);
        };
        function imageIsLoaded_1(e) {
            $('.content_pic_2').attr('src', e.target.result);
        };
        function imageIsLoaded_2(e) {
            $('.content_pic_3').attr('src', e.target.result);
        };
        function imageIsLoaded_3(e) {
            $('.content_pic_4').attr('src', e.target.result);
        };
        function imageIsLoaded_4(e) {
            $('.content_pic_5').attr('src', e.target.result);
        };
        function imageIsLoaded_5(e) {
            $('.content_pic_6').attr('src', e.target.result);
        };
        function imageIsLoaded_6(e) {
            $('.content_pic_7').attr('src', e.target.result);
        };
        function imageIsLoaded_7(e) {
            $('.content_pic_8').attr('src', e.target.result);
        };
        function imageIsLoaded_8(e) {
            $('.content_pic_9').attr('src', e.target.result);
        };
        function imageIsLoaded_9(e) {
            $('.content_pic_10').attr('src', e.target.result);
        };
        
    </script>
    <script src="{!! url("/resources/assets/ckeditor/ckeditor.js") !!}"></script>
    <script>
        CKEDITOR.replace('ckeditor');
    </script>
@endsection