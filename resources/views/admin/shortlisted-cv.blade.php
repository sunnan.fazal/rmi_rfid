@extends('admin.base')
@section("content")
    <style>
        body{
            overflow: hidden;
        }
    </style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Candidates CV
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card student-list">
                        <div class="body">
                            <div class="candidatesCV" style="height: 88vh"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            $(".candidatesCV").kendoGrid({
                toolbar: ["pdf","excel"],
                excel: {
                    fileName: "CVs.xlsx",
                    filterable: true
                },
                dataSource: {
                    transport: {
                        read: {
                            url : $("body").attr("data-url")+"/show-all-shortlisted-cv",
                            dataType : "json"
                        },
                    },
                    schema: {
                        data : "data",
                        total : "total"
                    },

                    batch: true,
                    pageSize: 25,
                    serverPaging: true,
                    serverFiltering: false,
                    serverSorting: false
                },
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            contains: "Contains",
                            doesnotcontain:"Doesn't Contain",
                            gt: "After",
                            lt: "Before"
                        }
                    }
                },
                groupable: true,
                sortable: true,
                resizable: true,
                pageable: {
                    pageSizes: true,
                    pageSizes: [ 25,50,100,500,1000],
                    refresh: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        template: "#if(CANDIDATE_PIC==null){#<img class='rounded-circle' src='"+$("body").attr("data-url")+"/resources/assets/icon-person.png'  style='width: 3em;height: 3em;'> #}else{# <img class='rounded-circle'  src='"+$("body").attr("data-url")+"/storage/app/pics/#:CANDIDATE_PIC#'  style='width: 3em;height: 3em;'> #}#",
                        field: "CANDIDATE_PIC",
                        title: "Image",
                        width : 70,
                        "filterable": {
                            cell :{
                                enabled: false,
                            }
                        }
                    },{
                        template: "<a target='_blank' style='font-weight: bold;color:rgb(31,42,91);' href='"+$("body").attr("data-url")+"/view-profile/#:ID#'>#:FULL_NAME#</a>",
                        field: "FULL_NAME",
                        title: "Full Name",
                    },{
                        field: "FATHER_SPOUSE",
                        title: "Father/Spouse",
                    },{
                        field: "JOB_TITLE",
                        title: "Job",
                    },{
                        field: "CATEGORY_NAME",
                        title: "Category",
                    },{
                        field: "MOBILE_NUM",
                        title: "Mobile #",
                    },{
                        field: "EMAIL",
                        title: "Email",
                    },{
                        field: "GENDER",
                        title: "Gender",
                    },{
                        field: "CITY",
                        title: "City",
                    },{
                        field: "POSTED_AT",
                        title: "Posted On",
                    },{
                        field: "APPLIED_ON",
                        title: "Applied On",
                    },{
                        template: "<div style='text-align: center;'>#if(CANDIDATE_CV!=null){# <a target='_blank' href='"+$("body").attr("data-url")+"/storage/app/cv/#:CANDIDATE_CV#'><i class='zmdi zmdi-eye' style='color:rgb(31,42,91);font-size: 2em;margin: 6px;'></i></a> #}#</div>",
                        field: "CANDIDATE_CV",
                        title: "View CV",
                        width: 80,
                        "filterable": {
                            cell :{
                                enabled: false,
                            }
                        }
                    },{
                        template: "#if(RATING == '1'){# <i class='zmdi zmdi-star'></i> #}#" +
                            "#if(RATING == '2'){# <i class='zmdi zmdi-star'></i><i class='zmdi zmdi-star'></i> #}#" +
                            "#if(RATING == '3'){# <i class='zmdi zmdi-star'></i><i class='zmdi zmdi-star'></i><i class='zmdi zmdi-star'></i> #}#" +
                            "#if(RATING == '4'){# <i class='zmdi zmdi-star'></i><i class='zmdi zmdi-star'></i><i class='zmdi zmdi-star'></i><i class='zmdi zmdi-star'></i> #}#" +
                            "#if(RATING == '5'){# <i class='zmdi zmdi-star'></i><i class='zmdi zmdi-star'></i><i class='zmdi zmdi-star'></i><i class='zmdi zmdi-star'></i><i class='zmdi zmdi-star'></i> #}#",
                        field: "RATING",
                        title: "Rating",
                    },{
                        template: "<span class='badge badge-success'>#:STATUS#</span>",
                        field: "STATUS",
                        title: "Status",
                        "filterable": {
                            cell :{
                                enabled: false,
                            }
                        }
                    }
                ]
            });
        });
    </script>
@endsection