@extends('admin.base')
@section("content")

    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Job Types
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong>Basic</strong> Information</h2>
                        </div>
                        @if(! isset($jobType))
                            <div class="body">
                                <form action="{!! url("save-job-type") !!}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <label for="">Enter Job Type<span style="color:red">*</span></label>
                                                <input type="text" class="form-control square" name="jobType" max="255" placeholder="Job Type" required>
                                                @if($errors->any())
                                                    <p style="color: red;">
                                                        Job Type <b>{!! old('jobType') !!}</b> already Exists !
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton">Save Job Type</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @elseif (isset($jobType))
                            <div class="body">
                                <form action="{!! url("edit-save-job-type") !!}" method="post">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="jobTypeID" value="{!! $jobType->ID !!}">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <label for="">Enter Job Type <span style="color:red">*</span></label>
                                                <input type="text" class="form-control square" name="jobType" max="255" value="{!! $jobType->JOB_TYPE_NAME !!}" placeholder="Job Type" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton">Update Job Type</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection