@extends('admin.base')
@section("content")

    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Create Advertisement
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <form action="{!! url("/save-advertisement") !!}" method="post" enctype="multipart/form-data">
                            <div class="header">
                                <h2><strong>Basic</strong> Information</h2>

                            </div>
                            <div class="body">

                                {!! csrf_field() !!}
                                <div class="row clearfix">
                                    <div class="col-sm-9">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="" style="float: right"><b>Active</b></label>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" name="active" style="" checked>
                                    </div>
                                </div>
                                <hr>
                                <div class="row clearfix">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Advertisement Description <span style="color:red">*</span></label>
                                            <input type="text" class="form-control square" name="career_description" placeholder="Description" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Type <span style="color:red">*</span></label>
                                            <select name="career_type" class="form-control">
                                                <option value="Contract">Contract</option>
                                                <option value="Permanent">Permanent</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Advertisement Date <span style="color:red">*</span></label>
                                            <input type="date" class="form-control square" name="advertisement_date" placeholder="Advertisement Date" required>
                                        </div>
                                    </div><div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Closing Date <span style="color:red">*</span></label>
                                            <input type="date" class="form-control square" name="closing_date" placeholder="Closing Date">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="">Attachment <span style="color:red">*</span></label>
                                            <input type="file" name="file" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton"><b>Submit Advertisement</b></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection