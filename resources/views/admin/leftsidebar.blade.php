<aside id="leftsidebar" class="sidebar" style="width: 15em;">
    <div class="tab-content">
        <div class="tab-pane active" id="dashboard">
            <div class="menu">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: calc(100vh - 60px);"><ul class="list" style="overflow: hidden; width: auto; height: calc(100vh - 60px);">
                        <li>
                            <div class="user-info">
                                <div><a href="{!! url("/") !!}" class=" waves-effect waves-block"><img src="{!! url("/resources/logo.jpeg") !!}" alt="PIC" style="width: 5em;"></a></div>
                                <div class="detail">
                                    <h6 style="font-size: 12px;">Hello, {!! explode("@",\Illuminate\Support\Facades\Auth::user()->username)[0] !!}</h6>
                                </div>
                            </div>
                        </li>
                        <li><a href="{!! url("/") !!}" class="toggled waves-effect waves-block"><span>Dashboard</span></a></li>
                        <li><a href="{!! url("/about-us") !!}" class="toggled waves-effect waves-block"><span>About Us</span></a></li>
                        <li><a href="{!! url("/governors") !!}" class="toggled waves-effect waves-block"><span>Board of Governors</span></a></li>
                        <li><a href="{!! url("/careers") !!}" class="toggled waves-effect waves-block"><span>Careers</span></a></li>
                        <li><a href="{!! url("/downloads") !!}" class="toggled waves-effect waves-block"><span>Downloads</span></a></li>
                        <li><a href="{!! url("/doctors") !!}" class="toggled waves-effect waves-block"><span>Doctors</span></a></li>
                        <li><a href="{!! url("/front-contents") !!}" class="toggled waves-effect waves-block"><span>Front Contents</span></a></li>
                        <li><a href="{!! url("/news") !!}" class="toggled waves-effect waves-block"><span>Latest News</span></a></li>
                        <li><a href="{!! url("/mti-acts") !!}" class="toggled waves-effect waves-block"><span>Rules & Regulations</span></a></li>
                        <li><a href="{!! url("/sliders") !!}" class="toggled waves-effect waves-block"><span>Sliders</span></a></li>
                        <li><a href="{!! url("/tenders") !!}" class="toggled waves-effect waves-block"><span>Tenders</span></a></li>
                        <li><a href="{!! url("/timeline") !!}" class="toggled waves-effect waves-block"><span>Timeline</span></a></li>
                        <li><a href="{!! url("/vision-mission") !!}" class="toggled waves-effect waves-block"><span>Vision & Mission</span></a></li>
                    </ul>
            </div>
        </div>
    </div>
</aside>