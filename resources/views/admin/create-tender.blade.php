@extends('admin.base')
@section("content")

    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Create Tender
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <form action="{!! url("/save-tender") !!}" method="post" enctype="multipart/form-data">
                            <div class="header">
                                <h2><strong>Basic</strong> Information</h2>

                            </div>
                            <div class="body">

                                {!! csrf_field() !!}
                                <div class="row clearfix">
                                    <div class="col-sm-9">
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="" style="float: right"><b>Active</b></label>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" name="active" style="" checked>
                                    </div>
                                </div>
                                <hr>
                                <div class="row clearfix">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Enter Tender Name <span style="color:red">*</span></label>
                                            <input type="text" class="form-control square" name="tenderName" placeholder="Tender Name" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Procurement Entity <span style="color:red">*</span></label>
                                            <input type="text" class="form-control square" name="procurement_entity" placeholder="Procurement Entity">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Advertisement Date <span style="color:red">*</span></label>
                                            <input type="date" class="form-control square" name="advertisement_date" placeholder="Advertisement Date" required>
                                        </div>
                                    </div><div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Closing Date <span style="color:red">*</span></label>
                                            <input type="date" class="form-control square" name="closing_date" placeholder="Closing Date">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="">Enter Tender Description <span style="color:red">*</span></label>
                                            <textarea name="tenderDescription" class="form-control tenderDescription" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="">Financial Remarks <span style="color:red">*</span></label>
                                            <textarea name="financialRemarks" class="form-control financialRemarks"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Tender Advertisement <span style="color:red">*</span></label>
                                            <input type="file" name="file" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Corrigendum<span style="color:red">*</span></label>
                                            <input type="file" name="file_2" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">SBD<span style="color:red">*</span></label>
                                            <input type="file" name="sbd" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">Technical Evaluation<span style="color:red">*</span></label>
                                            <input type="file" name="tender_evaluation" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton"><b>Submit Tender</b></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{!! url("/resources/assets/ckeditor/ckeditor.js") !!}"></script>
    <script>
        CKEDITOR.replace('tenderDescription');
        CKEDITOR.replace('financialRemarks');
    </script>
@endsection