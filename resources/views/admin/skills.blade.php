<div class="col-sm-6">
    <div class="header" style="background-color: #f4f4f4;">
        <h2><strong>Skills</strong></h2>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-10"></div>
        <div class="col-sm-2">
            <a class="addRowSkill"><i class="material-icons" style="color: lightgreen;font-size: 3em;cursor: pointer;">add_box</i></a>
        </div>
    </div>
    <div class="skills">
        @if($skill)
            @foreach($skill as $skills)
                <div class="row clearfix skillROW">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label for="" style="font-size: 0.9em;">Skill</label>
                            <input type="text" placeholder="Skill" class="form-control square" name="skill[]" value="{!! $skills->SKILL_NAME !!}">
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label for="" style="font-size: 0.9em;">Experience</label>
                            <select name="skillExperience[]" id="" class="form-control square">
                                <option {!! ($skills->EXPERIENCE == "-1") ? "selected" : "" !!} value="-1">Less than 1 Year Experience</option>
                                <option {!! ($skills->EXPERIENCE == "1") ? "selected" : "" !!} value="1">1 Year Experience</option>
                                <option {!! ($skills->EXPERIENCE == "2") ? "selected" : "" !!} value="2">2 Years Experience</option>
                                <option {!! ($skills->EXPERIENCE == "3") ? "selected" : "" !!} value="3">3 Years Experience</option>
                                <option {!! ($skills->EXPERIENCE == "4") ? "selected" : "" !!} value="4">4 Years Experience</option>
                                <option {!! ($skills->EXPERIENCE == "5+") ? "selected" : "" !!} value="5+">5+ Years Experience</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a data-id="{!! $skills->ID !!}" onclick="removeRowSkill(this)"><i class="material-icons" style="color: lightcoral;font-size: 3em;margin-top: 0.6em;cursor:pointer;">indeterminate_check_box</i></a>
                    </div>
                    <br>
                </div>
            @endforeach
        @endif
    </div>
    <script type="text/html" id="skillROWS">
        <div class="row clearfix skillROW">
            <div class="col-sm-5">
                <div class="form-group">
                    <label for="" style="font-size: 0.9em;">Skill</label>
                    <input type="text" placeholder="Skill" class="form-control square" name="skill[]">
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <label for="" style="font-size: 0.9em;">Experience</label>
                    <select name="skillExperience[]" id="" class="form-control square">
                        <option value="-1">Less than 1 Year Experience</option>
                        <option value="1">1 Year Experience</option>
                        <option value="2">2 Years Experience</option>
                        <option value="3">3 Years Experience</option>
                        <option value="4">4 Years Experience</option>
                        <option value="5+">5+ Years Experience</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <a data-id="" onclick="removeRowSkill(this)"><i class="material-icons" style="color: lightcoral;font-size: 3em;margin-top: 0.6em;cursor:pointer;">indeterminate_check_box</i></a>
            </div>
            <br>
        </div>
    </script>
    <script>
        $(document).ready(function () {
            $(".skills").append($("#skillROWS").html());
            $(".addRowSkill").on("click" ,function(){
                $(".skills").append($("#skillROWS").html());
            });
        });
        function removeRowSkill(val)
        {
            if($(val).attr("data-id") != "")
            {
                $.get($("body").attr("data-url")+"/remove-skill/"+$(val).attr("data-id"), function(data, status){
                    $(val).closest("div.skillROW").remove();
                });
            }
            $(val).closest("div.skillROW").remove();
        }
    </script>
</div>
