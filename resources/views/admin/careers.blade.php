@extends('admin.base')
@section("content")
    <style>
        body{
            overflow: hidden;
        }
    </style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>All Careers
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="body">
                            <a href="{{ url('/create-advertisement') }}" class="btn btn-primary" style="float: right">Create Advertisement</a>
                            <br>
                            <br>
                            <br>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Adv Id</th>
                                    <th>Description</th>
                                    <th>Type</th>
                                    <th>Advertised at</th>
                                    <th>Closing</th>
                                    <th>Attachment</th>
                                    <th>Active</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $careers as $tend)
                                    <tr>
                                        <td>{!! $tend->id !!}</td>
                                        <td>{!! $tend->description !!}</td>
                                        <td>{!! $tend->type !!}</td>
                                        <td>{!! date('d M, Y' , strtotime($tend->advertisement_date)) !!}</td>
                                        <td>{!! date('d M,Y' , strtotime($tend->closing_date)) !!}</td>
                                        <td><a target="_blank"  href="{!! url('resources/careers/'.$tend->file) !!}">Document</a></td>
                                        <td><input type="checkbox" {!! $tend->active == 1 ? 'checked' : '' !!}></td>
                                        <td><a style="color: darkblue" href="{!! url('/edit-career/' . $tend->id) !!}"><i class="zmdi zmdi-edit"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            $('.table').DataTable();
        });
    </script>

@endsection