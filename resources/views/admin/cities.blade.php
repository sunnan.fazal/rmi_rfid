@extends('admin.base')
@section("content")

    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Cities
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>RMI Jobs Cities</strong> List</h2>
                            <ul class="header-dropdown">
                                <a href="{!! url("/add-city") !!}" class="rmiDarkButton btn btn-primary square ">Create City</a>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0 cities">
                                    <thead>
                                    <tr>
                                        <th>City Name</th>
                                        <th>Created By</th>
                                        <th>Created At</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($cities as $city)
                                        <tr>
                                            <td>{!! $city->CITY_NAME !!}</td>
                                            <td>{!! $city->CREATED_BY !!}</td>
                                            <td>{!! $city->CREATED_AT !!}</td>
                                            <td>
                                                <a href="{!! url("/edit-city/".$city->ID) !!}" class="btn btn-warning">Edit</a>
                                                {{--<a href="{!! url("/delete-city/".$city->ID) !!}" class="btn btn-danger">Delete</a>--}}
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            $('.cities').DataTable();
        });
    </script>
@endsection