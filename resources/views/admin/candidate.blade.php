@extends('admin.base')
@section("content")
    <section class="content home" style="margin: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Dashboard
                        <small>Welcome to RMI Candidate Dashboard</small>
                    </h2>
                </div>

                <div class="col-sm-2" style="text-align: right;">
                    <a href="{!! url("/") !!}"><h6 style="color: white;margin-top: 1em;margin-left: 1em;">Search Jobs</h6></a>
                </div>
                <div class="col-sm-2" style="text-align: center;">
                    <a href="{!! url("/user-profile") !!}"><h6 style="color: white;margin-top: 1em;margin-left: 1em;">{!! ($profile->DOB == NULL || ($profile->ADDRESS == "" || $profile->ADDRESS == NULL) || ($profile->NATIONALITY == "" || $profile->NATIONALITY == NULL)) ? "Create Profile" : "View Profile" !!}</h6></a>
                </div>
                <div class="col-sm-2" style="display:flex;">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;margin-top: 1em;margin-left: 1em;font-weight: bold"><i class="zmdi zmdi-power"></i></h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-xl-6 col-lg-7 col-md-12">
                    <div class="card profile-header">
                        <div class="body">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-12">
                                    <div class="profile-image float-md-right">
                                        <img class="img-raised" src="{!! ($profile->CANDIDATE_PIC == NULL || $profile->CANDIDATE_PIC == "") ? url("/resources/assets/icon-person.png") : url("/storage/app/pics/".$profile->CANDIDATE_PIC) !!}" alt="" style="height: 13em;"> </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-12">
                                    <h4 class="m-t-0 m-b-0"><strong>{!! $profile->FIRST_NAME !!}</strong> {!! $profile->LAST_NAME !!}</h4>
                                    <p>{!! $profile->ADDRESS !!}
                                        <br>
                                        <span class="job_post">{!! $profile->CITY !!}, {!! $profile->COUNTRY !!}</span>
                                    </p>
                                    <p>
                                        <span class="job_post">Member Since - {!! date("M d, Y" , strtotime($profile->CREATED_AT))  !!}</span>
                                    </p>
                                    <p>
                                        <span class="job_post">Last Apply -
                                        <?php
                                            foreach($lastJob as $d)
                                            {
                                                foreach($d as $a)
                                                {
                                                    echo date("M d, Y", strtotime($a->createDate)).', '.$a->title;
                                                }
                                            }
                                            ?>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-5 col-md-12">
                    <div class="card">
                        <ul class="row profile_state list-unstyled">
                            <li class="col-lg-6">
                                <div class="body lowPadding">
                                    <i class="zmdi zmdi-account text-success"></i>
                                    <h5 class="m-b-0 number count-to" data-from="0" data-to="2365" data-speed="1000" data-fresh-interval="700">
                                        <?php
                                        foreach($available as $d)
                                        {
                                            foreach($d as $a)
                                            {
                                                echo $a->jobs;
                                            }
                                        }
                                     ?></h5>
                                    <small>Available Jobs</small>
                                </div>
                            </li>
                            <li class="col-lg-6">
                                <div class="body lowPadding">
                                    <i class="zmdi zmdi-camera col-amber"></i>
                                    <h5 class="m-b-0 number count-to" data-from="0" data-to="1203" data-speed="1000" data-fresh-interval="700">{!! $profile->CANDIDATE_PIC == NULL || $profile->CANDIDATE_PIC == "" ? 0 : 1 !!}</h5>
                                    <small>Image</small>
                                </div>
                            </li>
                            <li class="col-lg-6">
                                <div class="body lowPadding">
                                    <i class="zmdi zmdi-comment-text col-red"></i>
                                    <h5 class="m-b-0 number count-to" data-from="0" data-to="1980" data-speed="1000" data-fresh-interval="700">{!! count($candidate) !!}</h5>
                                    <small>Apply</small>
                                </div>
                            </li>
                            <li class="col-lg-6">
                                <div class="body lowPadding">
                                    <i class="zmdi zmdi-attachment text-warning"></i>
                                    <h5 class="m-b-0 number count-to" data-from="0" data-to="251" data-speed="1000" data-fresh-interval="700">{!! $profile->CANDIDATE_CV == NULL || $profile->CANDIDATE_CV == "" ? 0 : 1 !!}</h5>
                                    <small>Uploaded CV</small>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                @foreach($activeJobs as $data)
                    @foreach($data as $active)
                    <div class="col-lg-3 col-md-3">
                        <div class="card top_counter">
                            <div class="body">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <a href="{!! url("/search-job-category/".$active->CATEGORY_ID) !!}"><div class="text">{!! $active->CATEGORY_NAME !!}</div></a>
                                    <h5 class="number count-to" data-from="0" data-to="{!! $active->jobs !!}" data-speed="2500" data-fresh-interval="700">{!! $active->jobs !!}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endforeach
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>Applied</strong> Jobs List <small>The jobs you have applied are showing below</small></h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0">
                                    <thead>
                                    <tr>
                                        <th>Job Title</th>
                                        <th>Job Category</th>
                                        <th>Job Posted</th>
                                        <th>Apply Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($candidate as $can)
                                        <tr>
                                            <td><span class="list-name">{!! $can->advertisement->JOB_TITLE !!}</span></td>
                                            <td>{!! $can->advertisement->category->CATEGORY_NAME !!}</td>
                                            <td>{!! $can->advertisement->POSTED_AT !!}</td>
                                            <td>{!! $can->CREATED_AT !!}</td>
                                           {{-- <td>
                                                @if($can->STATUS == "Pending")
                                                    <span class="badge badge-warning">{!! $can->STATUS !!}</span>
                                                @elseif($can->STATUS == "Shortlisted")
                                                    <span class="badge badge-success">{!! $can->STATUS !!}</span>
                                                @elseif($can->STATUS == "Rejected")
                                                    <span class="badge badge-danger">{!! $can->STATUS !!}</span>
                                                @endif
                                            </td>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
