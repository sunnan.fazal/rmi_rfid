@extends('admin.base')
@section("content")
    
<style type="text/css">
    .content_pic_1 {
        width: 300px;
        height:  200px;
    }
</style>
    <section class="content home" style="margin-top: 0px">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-1">
                    <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style="color: white;"><i class="zmdi zmdi-swap"></i></a>
                </div>
                <div class="col-sm-9">
                    <h2>Edit Doctor
                    </h2>
                </div>
                <div class="col-sm-2">
                    <a href="{!! url("/logout") !!}"><h6 style="color: white;">Logout</h6></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <form action="{!! url("/edit-save-doctor") !!}" method="post" enctype="multipart/form-data">
                            <div class="header">
                                <h2><strong>Basic</strong> Information</h2>

                            </div>
                            <div class="body">
                                {!! csrf_field() !!}
                                <input type="hidden" name="doctorid" value="{{ $doctor->id }}">
                                <div class="row clearfix">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <img class="pull-left flip mr-15 thumbnail content_pic_1"  src="{{ url('/resources/doctors/' . $doctor->doctor_image) }}" alt="">
                                            <input type="file" name="pic" class="form-control pic_1" >
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" name="name" class="form-control" value="{!! $doctor->name !!}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" name="title" class="form-control" value="{!! $doctor->title !!}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <textarea class="form-control ckeditor" name="description" required="">{!! $doctor->description !!}</textarea>
                                    </div>
                                </div>
                                <br>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <button type="submit" class=" btn btn-raised btn-round square rmiDarkButton"><b>Save Doctor</b></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(function () {
            $(".pic_1").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                }
            });
       });

        function imageIsLoaded(e) {
            $('.content_pic_1').attr('src', e.target.result);
        };
        
    </script>
    <script src="{!! url("/resources/assets/ckeditor/ckeditor.js") !!}"></script>
    <script>
        CKEDITOR.replace('ckeditor');
    </script>
@endsection