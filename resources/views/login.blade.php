<!DOCTYPE html>
<html>
<head>
    <title>CMS - Peshawar Institute of Cardiology</title>

     <link rel="stylesheet" type="text/css" href="{!! url("/resources/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css") !!}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! url("/resources/assets/fonts/iconic/css/material-design-iconic-font.min.css") !!}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! url("/resources/assets/vendor/animate/animate.css") !!}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! url("/resources/assets/vendor/animsition/css/animsition.min.css") !!}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! url("/resources/assets/css/util.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("/resources/assets/css/main.css") !!}">
    <!--===============================================================================================-->
    <style>
        a:hover
        {
            background: transparent !important;
        }
        input
        {
            color: white !important;
            font-weight: bold !important;
        }
    </style>
</head>
<body>
<div class="limiter">
        <div class="container-login100" style="background-image: url('{!! url("/resources/background.jpg") !!}');">
            <div class="wrap-login100">
                <form class="login100-form validate-form" action="{!! url("/login") !!}" method="post">
                    {!! csrf_field() !!}
                    <span class="login100-form-title p-b-34 p-t-27" style="font-size: 27px">
                        Peshawar Institute of Cardiology
                    </span>
                    <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            <span style="color: red;font-size: 2em">* </span> <span style="color: white">{!! $error !!}</span>
                        </li>
                    @endforeach
                    </ul>
                    <div class="wrap-input100 validate-input" data-validate = "Enter Email">
                        <input class="input100" type="email" name="username" placeholder="Email">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" type="password" name="password" placeholder="Password">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
